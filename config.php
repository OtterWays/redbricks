<?php
//Fichier de configurations spécifiques qui écrase les configurations générales
return [
    'database.host' => 'localhost',
    'database.username' => 'root',
    'database.password' => '',
    'database.name' => 'redbricks',
    'stripe.key' => 'public_key',
    'stripe.secret' => 'secret_key',
	'facebook.appId' => 'facebook_app_id',
	'facebook.appSecret' => 'facebook_app_secret'
];