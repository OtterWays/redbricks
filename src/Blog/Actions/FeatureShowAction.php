<?php

namespace App\Blog\Actions;

use App\Blog\Table\FeatureTable;
use App\Blog\Table\StatusTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;
use Framework\Auth;

class FeatureShowAction {

    protected $viewPath = "@blog/user/features/feature";
    protected $routePrefix = "blog.user.features.feature";

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var featureTable
     */
    private $featureTable;

    /**
     * @var statusTable
     */
    private $statusTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, FeatureTable $featureTable, StatusTable $statusTable, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->featureTable = $featureTable;
        $this->statusTable = $statusTable;
        $this->auth = $auth;
    }

    /**
     * Affiche la fonctionnalité d'un jeu
     * @param Request $request
     * @return  ResponseInterface/string
     */
    public function __invoke(Request $request) {
        $slug = $request->getAttribute("slug");
        $project_id = $request->getAttribute("id");
        $params["status"] = $this->statusTable->findList();
        if ($slug == "null") {
            //On est en création
            return $this->renderer->render("@blog/user/features/feature/create", [
                        "project_id" => $project_id,
                        "status" => $params["status"],
                        "viewPath" => $this->viewPath,
                        "routePrefix" => $this->routePrefix
            ]);
        }
        $feature = $this->featureTable->findShowFeature($project_id, $slug);
        //var_dump($feature);die();

        return $this->renderer->render("@blog/user/features/feature/edit", [
                    "status" => $params["status"],
                    "feature" => $feature,
                    "viewPath" => $this->viewPath,
                    "routePrefix" => $this->routePrefix
        ]);
    }

}
