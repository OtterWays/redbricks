<?php

namespace App\Blog\Actions;

use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Shop\Table\SubscriptionTable;
use App\Blog\Table\FeatureTable;
use App\Blog\Table\GameTable;
use App\Bug\Table\BugTable;
use App\Blog\Table\SocialMediaTable;
use App\Comments\Table\CommentsTable;
use App\News\Table\NewsTable;
use App\Auth\UserTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Survey\Table\SurveyTable;
use App\Shop\Table\RewardTable;
use Framework\Database\NoRecordException;
use Framework\Auth;

class GameShowAction {

    protected $routePrefix = "blog";

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var GameTable
     */
    private $gameTable;

    /**
     * @var PendingPurchaseTable
     */
    private $pendingPurchaseTable;

    /**
     *
     * @var PurchaseTable
     */
    protected $purchaseTable;

    /**
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var BugTable
     */
    protected $bugTable;

    /**
     *
     * @var SocialMediaTable
     */
    protected $socialMediaTable;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     *
     * @var NewsTable
     */
    protected $newsTable;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var SurveyTable
     */
    protected $surveyTable;

    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;

    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, FeatureTable $featureTable, PurchaseTable $purchaseTable, PendingPurchaseTable $pendingPurchaseTable, SubscriptionTable $subscriptionTable, BugTable $bugTable, SocialMediaTable $socialMediaTable, CommentsTable $commentsTable, NewsTable $newsTable, UserTable $userTable, SurveyTable $surveyTable, RewardTable $rewardTable, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->purchaseTable = $purchaseTable;
        $this->bugTable = $bugTable;
        $this->socialMediaTable = $socialMediaTable;
        $this->commentsTable = $commentsTable;
        $this->newsTable = $newsTable;
        $this->userTable = $userTable;
        $this->surveyTable = $surveyTable;
        $this->rewardTable = $rewardTable;
        $this->subscriptionTable = $subscriptionTable;
        $this->auth = $auth;
    }

    /**
     * Affiche un élément
     * @param Request $request
     * @return  ResponseInterface/string
     */
    public function __invoke(Request $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);
        $liens = $this->gameTable->findShowPlatform($gameId)->fetchAll();
        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $this->bugTable->findShow($gameId)->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $this->newsTable->findShow($gameId)->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");
        $medias = $this->socialMediaTable->findCompletedByGame($gameId);
        $lastComments = $this->commentsTable->findLast($gameId, 3);
        $stripeUserId = $this->userTable->find($game->getUserId())->getStripeUserId();

        $moneyRaisedPending = $this->pendingPurchaseTable->getMoneyRaised("game_id", $gameId);
        $moneyRaised = $this->purchaseTable->getMoneyRaised("game_id", $gameId);
        $subscriptions = $this->subscriptionTable->getMoneySubscripted("game_id", $gameId);
        $countSubscriber = $this->subscriptionTable->countSubscribers("game_id", $gameId);
        $countContributorPending = $this->pendingPurchaseTable->countParticipants("game_id", $gameId);
        $countContributor = $this->purchaseTable->countParticipants("game_id", $gameId);
        $rewards = $this->rewardTable->findAllBy("project_id", $gameId);
        $user = $this->auth->getUser();
        if (is_null($user)) {
            $subscriptionUser = null;
        } else {
            try {
                $subscriptionUser = $this->subscriptionTable->findByUserGame($user->getId(), $gameId);
            } catch (NoRecordException $e) {
                $subscriptionUser = null;
            }
        }

        if ($game->getSlug() !== $slug) {
            return $this->redirect("blog.show", [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        }

        return $this->renderer->render("@blog/show", [
                    "game" => $game,
                    "liens" => $liens,
                    "moneyRaised" => $moneyRaised,
                    "moneyRaisedPending" => $moneyRaisedPending,
                    "subscriptions" => $subscriptions,
                    "countContributor" => $countContributor,
                    "countContributorPending" => $countContributorPending,
                    "countSubscriber" => $countSubscriber,
                    "medias" => $medias,
                    "lastComments" => $lastComments,
                    "stripeUserId" => $stripeUserId,
                    "nbFeatures" => $nbFeatures,
                    "nbBugs" => $nbBugs,
                    "nbComments" => $nbComments,
                    "nbNews" => $nbNews,
                    "nbSurveys" => $nbSurveys,
                    "rewards" => $rewards,
                    "subscriptionUser" => $subscriptionUser
        ]);
    }

}
