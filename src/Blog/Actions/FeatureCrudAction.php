<?php

namespace App\Blog\Actions;

use TreeHouse\Slugifier\Slugifier;
use Framework\Database\Hydrator;
use App\Blog\Entity\Feature;
use App\Blog\FeatureUpload;
use App\Blog\Table\StatusTable;
use App\Blog\Table\FeatureTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\VotesTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Blog\Table\FeatureContentTable;
use App\Blog\Table\ContributorTable;
use App\Blog\Table\TagTable;
use App\Blog\Table\FeatureTagTable;
use App\Shop\PurchaseProduct;
use App\Notification\SendNotification;
use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\SessionInterface;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Auth;

class FeatureCrudAction extends CrudAction {

    protected $viewPath = "@blog/user/features/feature";
    protected $routePrefix = "blog.user.features";

    /**
     * @var statusTable
     */
    private $statusTable;

    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     *
     * @var PurchaseProduct
     */
    protected $purchaseProduct;

    /**
     *
     * @var FeatureContentTable
     */
    protected $featureContentTable;

    /**
     *
     * @var VotesTable
     */
    protected $votesTable;

    /**
     *
     * @var TagTable
     */
    protected $tagTable;

    /**
     *
     * @var FeatureTagTable
     */
    protected $featureTagTable;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;
    public $projectId;
    protected $acceptedParams = ["slug", "project_id", "status_id", "goal", "abandonReason", "deadline", "estimatedDate", "delivery_date", "abandonDate"];

    public function __construct(
            RendererInterface $renderer, Router $router, FeatureTable $table, GameTable $gameTable, StatusTable $statusTable, VotesTable $votesTable, PendingPurchaseTable $pendingPurchaseTable, FeatureContentTable $featureContentTable, ContributorTable $contributorTable, PurchaseProduct $purchaseProduct, SendNotification $sendNotification, FlashService $flash, SessionInterface $session, TagTable $tagTable, FeatureTagTable $featureTagTable
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->gameTable = $gameTable;
        $this->statusTable = $statusTable;
        $this->votesTable = $votesTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->purchaseProduct = $purchaseProduct;
        $this->sendNotification = $sendNotification;
        $this->session = $session;
        $this->featureContentTable = $featureContentTable;
        $this->contributorTable = $contributorTable;
        $this->tagTable = $tagTable;
        $this->featureTagTable = $featureTagTable;
        $this->messages = [
            "create" => _("The feature has been created. A moderator must to approve it to publishing."),
            "createMod" => _("The feature has been created."),
            "edit" => _("The feature has been modified.")
        ];
    }

    public function __invoke(ServerRequestInterface $request) {
        if (substr((string) $request->getUri(), -9) === "archiving") {
            return $this->archiving($request);
        }
        return parent::__invoke($request);
    }

    public function delete(ServerRequestInterface $request) {
        //var_dump($request);die();
        $featureId = $request->getAttribute('id');
        $projectId = $request->getAttribute('projectId');
        //var_dump($featureId,$projectId);die();
        $feature = $this->table->findEn($featureId);
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($feature->getUserId(), $game->getUserId(), $game->getId());
        $this->postPersist($request, $feature);
        $votes = $this->votesTable->findBy("feature_id", $featureId)->fetchAll();
        foreach ($votes->records as $vote) {
            $this->votesTable->delete($vote["id"]);
        }
        $featureUpload = new FeatureUpload($feature->getProjectId());
        $featureUpload->delete($feature->getImage());
        $this->table->delete($request->getAttribute("id"));
        $this->flash->success(_("successful deletion"));
        return $this->redirect($this->routePrefix, ['id' => $projectId]);
    }

    protected function formParams(array $params): array {
        $params["status"] = $this->statusTable->findList();
        return $params;
    }

    protected function prePersist(ServerRequestInterface $request, $feature): array {
        //var_dump($request, $feature);die();
        $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());
        $featureUpload = new FeatureUpload($feature->getProjectId());
        $image = $featureUpload->upload($params["image"], $feature->getImage());
        if ($image) {
            $params["image"] = $image;
            $this->acceptedParams[] = 'image';
        } else {
            unset($params["image"]);
        }
        //var_dump($this->session->get('auth.role'));die();
        if ($this->session->get('auth.role')) {
            $userId = $this->session->get('auth.user');
        } else {
            $userId = null;
        }
        if (isset($params["projectId"])) {
            $params["project_id"] = $params["projectId"];
        }
        if ($params["status_id"] == 5) {
            $params["delivery_date"] = $params["deliveryDate"];
        }
        if ($params["status_id"] == 6) {
            $params["abandonDate"] = date("Y-m-d H:i:s");
            if (!empty($params['id'])) {
                $this->pendingPurchaseTable->deleteAllByFeature($params['id']);
            }
        } else {
            unset($params["abandonReason"]);
        }
        if ($params["status_id"] != 3) {
            unset($params["deadline"]);
            unset($params["goal"]);
        }
        if ($params["status_id"] != 4) {
            unset($params["estimatedDate"]);
        }


        $slugifier = new Slugifier();
        $params["slug"] = $slugifier->slugify($params["name"]);

        $params = array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
        return array_merge($params, ["updated_at" => date("Y-m-d H:i:s"),
            "created_at" => $feature->getCreatedAt()->format("Y-m-d H:i:s"),
            "user_id" => $userId]);
    }

    protected function getNewEntity() {
        $feature = new Feature();
        $feature->setCreatedAt(new \DateTime());
        return $feature;
    }

    protected function getValidator(ServerRequestInterface $request) {

        $validator = parent::getValidator($request)
                ->required("name", "content", "status_id")
                ->length("name", 2, 50)
                ->length("content", 2)
                ->extension("image", ["jpg", "png", "gif", "bmp"])
                ->size("image", 1024 * 1024); //1Mo
        $params = $request->getParsedBody();
        if (!empty($params["status_id"]) && $params["status_id"] == 3) {
            $validator->duration("deadline", 1, 90)
                    ->number("goal", 100);
        } elseif (!empty($params["status_id"]) && $params["status_id"] == 4) {
            $validator->duration("estimatedDate", 1);
        } elseif (!empty($params["status_id"]) && $params["status_id"] == 5) {
            $validator->duration("deliveryDate", null, 0);
        } elseif (!empty($params["status_id"]) && $params["status_id"] == 6) {
            $validator->length("abandonReason", 2, 100);
        }
        //Si on veut rendre l'image obligatoire en création
        //En création, $request->getAttribute("id") est null
        /* if (is_null($request->getAttribute("id"))) {
          $validator->uploaded("image");
          } */
        return $validator;
    }

    /**
     * Edite une fonctionnalité
     * @param Request $request
     * @return ResponseInterface|string
     */
    public function edit(ServerRequestInterface $request) {
        $item = $this->table->findEn($request->getAttribute("id"));
        $game = $this->gameTable->find($item->getProjectId());
        $this->validateAccessRights($item->getUserId(), $game->getUserId(), $game->getId());
        $item->userProject = $game->getUserId();
        $moneyRaised = $this->pendingPurchaseTable->getMoneyRaised("p.feature_id", $item->getId());
        $disabled = ($item->getStatusId() > 3 || !empty($item->getGoal())) && $this->session->get('auth.role') != "admin";
        $endGoal = $item->getStatusId() == 3 && $moneyRaised >= $item->getGoal();
        $statusId = $item->getStatusId();
        $tags = $this->tagTable->findShow($game->getId());
        $featureTag = $this->featureTagTable->findListByFeature($item->getId());
        if ($request->getMethod() === "POST") {
            //echo '<pre>'; var_dump($request); echo '</pre>'; die();
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                //On enlève le user_id qui est le dernier élément
                $fields = $this->prePersist($request, $item);
                $params = $request->getParsedBody();
                array_pop($fields);

                $this->table->update($item->getId(), $fields);
                $this->featureContentTable->updateByFeature($item->getId(), [
                    "name" => $params['name'],
                    "content" => $params['content']
                ]);
                if ($params['status_id'] == 2) {
                    $this->votesTable->insertUnique([
                        "user_id" => $item->getUserId(),
                        "feature_id" => $item->getId(),
                        "vote" => 1
                    ]);
                }

                $this->flash->success($this->messages["edit"]);

                $this->postPersist($request, $item);
                $featureTag = $this->featureTagTable->findListByFeature($item->getId());

                $this->sendNotification->send('change_brick', [
                    "gameId" => $item->getProjectId(),
                    "featureId" => $item->getId(),
                    "list" => true
                ]);

                if ($item->getUserId() != $game->getUserId()) {
                    $this->sendNotification->send('changeContri', [
                        "gameId" => $game->getId(),
                        "featureId" => $item->getId(),
                        "userId" => $item->getUserId()
                    ]);
                }
                return $this->redirect($this->routePrefix, [
                            "id" => $item->getProjectId()
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }
        return $this->renderer->render(
                        $this->viewPath . "/edit", $this->formParams(compact("item", "errors", "disabled", "statusId", "endGoal", "tags", "featureTag"))
        );
    }

    /**
     * Crée un nouvel élément
     * @param Request $request
     * @return ResponseInterface|string
     */
    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $item->setProjectId($request->getAttribute('projectId'));
        $item->userProject = $this->gameTable->find($item->getProjectId())->getUserId();
        $item->projectSlug = $this->gameTable->find($item->getProjectId())->getSlug();
        $statusId = 0;
        $tags = $this->tagTable->findShow($item->getProjectId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $params = $request->getParsedBody();
                $this->table->insert($this->prePersist($request, $item));
                $featureId = $this->table->getPdo()->lastInsertId();
                $this->featureContentTable->insert([
                    "feature_id" => $featureId,
                    "name" => $params['name'],
                    "content" => $params['content'],
                    "langcode" => 'en'
                ]);
                if ($params['status_id'] == 2) {
                    $userId = $this->session->get('auth.user');
                    $this->votesTable->insert([
                        "user_id" => $userId,
                        "feature_id" => $featureId,
                        "vote" => 1
                    ]);
                }
                $this->postPersist($request, $item);
                $this->sendNotification->send('new_brick', [
                    "gameId" => $item->getProjectId(),
                    "featureId" => $featureId,
                    "list" => true
                ]);

                if ($item->userProject == $this->session->get('auth.user')) {
                    $this->flash->success($this->messages["createMod"]);
                    return $this->redirect($this->routePrefix, [
                                "id" => $item->getProjectId()
                    ]);
                } else {
                    $this->flash->success($this->messages["create"]);
                    return $this->redirect('blog.show.bricks', [
                                "slug" => $item->projectSlug,
                                "id" => $item->getProjectId()
                    ]);
                }
            }
            Hydrator::hydrate($request->getParsedBody(), $item);
            $errors = $validator->getErrors();
        }
        return $this->renderer->render(
                        $this->viewPath . "/create", $this->formParams(compact("item", "errors", "statusId", "tags"))
        );
    }

    protected function postPersist(ServerRequestInterface $request, $item): void {
        $game = $this->gameTable->find($item->getProjectId());
        $this->gameTable->update($game->getId(), [
            "updated_at" => date("Y-m-d H:i:s")
        ]);

        $tags = $this->tagTable->findShow($item->getProjectId());
        $params = $request->getParsedBody();
        foreach ($tags as $tag) {
            if (isset($params['tag-' . $tag->getId()])) {
                $this->featureTagTable->insertUnique([
                    "feature_id" => $item->getId(),
                    "tag_id" => $tag->getId()
                ]);
            } else {
                $this->featureTagTable->deleteByTag($item->getId(), $tag->getId());
            }
        }
    }

    protected function validateAccessRights(int $featureUserId, int $gameUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null ||
                ($role == 'user' && $userId != $featureUserId && $userId != $gameUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

    public function archiving(ServerRequestInterface $request) {
        $projectId = $request->getAttribute('projectId');
        $game = $this->gameTable->find($projectId);
        $features = $this->table->findShow($projectId);
        $params = $request->getParsedBody();
        $this->validateAccessRights(0, $game->getUserId(), $projectId);
        foreach ($features as $feature) {
            if (isset($params[$feature->getId()]) && !$feature->isArchived() && $feature->getStatusId() > 4) {
                $this->table->update($feature->getId(), [
                    "archived" => 1
                ]);
            } elseif (!isset($params[$feature->getId()]) && $feature->isArchived()) {
                $this->table->update($feature->getId(), [
                    "archived" => 0
                ]);
            }
        }
        return $this->redirect($this->routePrefix, [
                    "id" => $projectId
        ]);
    }

}
