<?php

namespace App\Blog\Actions;

use App\Blog\Table\FeatureTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\StatusTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Blog\Table\FeatureSummaryTable;
use App\Blog\Table\ContributorTable;
use Framework\Renderer\RendererInterface;
use Framework\Database\NoRecordException;
use Psr\Http\Message\ServerRequestInterface as Request;

class FeatureIndexAction {

    protected $viewPath = "@blog/user/features";
    protected $routePrefix = "blog.user.features.feature";

    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     *
     * @var FeatureSummaryTable
     */
    protected $featureSummaryTable;
    
    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, StatusTable $statusTable, FeatureTable $featureTable, PendingPurchaseTable $pendingPurchaseTable, FeatureSummaryTable $featureSummaryTable, ContributorTable $contributorTable
    ) {

        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->statusTable = $statusTable;
        $this->featureTable = $featureTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->featureSummaryTable = $featureSummaryTable;
        $this->contributorTable = $contributorTable;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $games = $this->gameTable->findShow($id);
        $features = $this->featureTable->findShow($id);
        $moneyRaised = $this->pendingPurchaseTable->getAllMoneyForProject($id);
        $contributors = $this->contributorTable->findByGameId($id);
        try {
            $summary = $this->featureSummaryTable->findShowEn($id);
        } catch (NoRecordException $e) {
            $summary = null;
        }
        return $this->renderer->render("@blog/user/features/index", compact("games", "features", "status", "moneyRaised", "summary", "contributors"));
    }

}
