<?php

namespace App\Blog\Actions;

use Framework\Response\RedirectResponse;
use Framework\Session\PHPSession;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;

class FilterAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
            RendererInterface $renderer,
            PHPSession $session
    ) {
        $this->renderer = $renderer;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request) {
        $uri = "/home/filter/";

        $params = $request->getParsedBody();

        if (isset($params["platform"]) or isset($params["licence"])) {
            $platforms_filter = (isset($params["platform"])) ? implode(",", $params["platform"]) : "";
            $licences_filter = (isset($params["licence"])) ? implode(",", $params["licence"]) : "";
            $filter = $platforms_filter . (($platforms_filter != "") ? (($licences_filter != "") ? "," : "") : "") . $licences_filter;
            return new RedirectResponse($uri . $filter);
        } else {
            $platforms_filter = "";
            return (new \GuzzleHttp\Psr7\Response())->withHeader('Location', substr($uri, 0, -7));
        }
    }

}
