<?php

namespace App\Blog\Actions;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Blog\Table\GameTable;
use App\Blog\Table\TagTable;

class TagIndexAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;
    
    /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var TagTable;
     */
    protected $tagTable;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, TagTable $tagTable
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->tagTable = $tagTable;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $games = $this->gameTable->findShow($id);
        $tags = $this->tagTable->findShowAllLang($id);
        
        return $this->renderer->render("@blog/user/tags/index", compact("games", "tags"));
    }

}
