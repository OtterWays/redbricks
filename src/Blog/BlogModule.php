<?php

namespace App\Blog;

use App\Blog\Actions\FeaturesCronAction;
use App\Blog\Actions\FeatureIndexAction;
use App\Blog\Actions\StatusCrudAction;
use App\Blog\Actions\SearchShowAction;
use App\Blog\Actions\SearchAction;
use App\Blog\Actions\FilterShowAction;
use App\Blog\Actions\FilterAction;
use App\Blog\Actions\LicenceCrudAction;
use App\Blog\Actions\PlatformCrudAction;
use App\Blog\Actions\GameUserCrudAction;
use App\Blog\Actions\GameCrudAction;
use App\Blog\Actions\GameIndexAction;
use App\Blog\Actions\GameShowAction;
use App\Blog\Actions\GameFeatureShowAction;
use App\Blog\Actions\GameFeatureVoteAction;
use Framework\Auth\RoleMiddlewareFactory;
use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;
use App\Blog\Actions\GameShowBricksAction;
use App\Blog\Actions\TagIndexAction;
use App\Blog\Actions\TagCrudAction;
use App\Blog\Actions\SummaryCrudAction;

class BlogModule extends Module {

    const DEFINITIONS = __DIR__ . "/config.php";
    const MIGRATIONS = __DIR__ . "/db/migrations";
    const SEEDS = __DIR__ . "/db/seeds";

    public function __construct(ContainerInterface $container) {
        $blogPrefix = $container->get("blog.prefix");
        $container->get(RendererInterface::class)->addPath("blog", __DIR__ . "/views");
        $router = $container->get(Router::class);
        $router->get($blogPrefix, GameIndexAction::class, "blog.index");
        $router->get("$blogPrefix/new", GameIndexAction::class, "blog.index.new");
        $router->get("$blogPrefix/random", GameIndexAction::class, "blog.index.random");
        $router->get("$blogPrefix/favorites", GameIndexAction::class, "blog.index.favorites");
        $router->post($blogPrefix, GameIndexAction::class);
        $router->post("$blogPrefix/new", GameIndexAction::class);
        $router->post("$blogPrefix/random", GameIndexAction::class);
        $router->post("$blogPrefix/favorites", GameIndexAction::class);
        $router->get("$blogPrefix/{slug:[a-z\-0-9]+}-{id:[0-9]+}", GameShowAction::class, "blog.show");
        $router->get("$blogPrefix/{slug:[a-z\-0-9]+}-{id:[0-9]+}/bricks", GameShowBricksAction::class, "blog.show.bricks");
        $router->get("$blogPrefix/{slug:[a-z\-0-9]+}-{id:[0-9]+}/bricks/{hideArchiving:hide|show}", GameShowBricksAction::class, "blog.show.bricks.archiving");
        $router->get("$blogPrefix/brick/{slug:[a-z\-0-9]+}-{id:[0-9]+}", GameFeatureShowAction::class, "blog.feature.showOneFeature");
        $router->get("$blogPrefix/brick/{slug:[a-z\-0-9]+}-{id:[0-9]+}/vote", [
            $container->get(RoleMiddlewareFactory::class)->makeForRole(['user']),
            GameFeatureVoteAction::class
                ], "blog.vote");
        $router->post("$blogPrefix/brick/{slug:[a-z\-0-9]+}-{id:[0-9]+}/vote", [
            $container->get(RoleMiddlewareFactory::class)->makeForRole(['user']),
            GameFeatureVoteAction::class
                ]);
        $router->get("$blogPrefix/cron", FeaturesCronAction::class, "blog.cron");
        $router->get("$blogPrefix/filter/{search:[a-z\,\:\-0-9]+}", FilterShowAction::class, "blog.filter");
        $router->post("$blogPrefix/filter/{search:[a-z\,\:\-0-9]+}", FilterAction::class);
        $router->get("$blogPrefix/search/{search: .*}", SearchShowAction::class, "blog.search");
        $router->post("$blogPrefix/search/{search: .*}", SearchAction::class);


        if ($container->has("admin.prefix")) {
            $prefix = $container->get("admin.prefix");
            $router->crud("$prefix/games", GameCrudAction::class, "blog.admin");
            $router->get("$prefix/bricks/{id:[0-9]+}", FeatureIndexAction::class, "blog.admin.features");
            $router->any("$prefix/bricks/{id:[0-9]+}/{slug:[a-z\-0-9]+}", Actions\FeatureShowAction::class, "blog.admin.features.feature");
            $router->crud("$prefix/bricks/brick/{projectId:[0-9]+}", Actions\FeatureCrudAction::class, "blog.admin.features.feature");
            $router->crud("$prefix/licences", LicenceCrudAction::class, "blog.licence.admin");
            $router->crud("$prefix/platforms", PlatformCrudAction::class, "blog.platform.admin");
            $router->crud("$prefix/status", StatusCrudAction::class, "blog.status.admin");
        }

        if ($container->has("user.prefix")) {
            $prefix = $container->get("user.prefix");
            $router->crud("$prefix/games", GameUserCrudAction::class, "blog.user");
            $router->get("$prefix/bricks/{id:[0-9]+}", FeatureIndexAction::class, "blog.user.features");
            $router->any("$prefix/bricks/{id:[0-9]+}/{slug:[a-z\-0-9]+}", Actions\FeatureShowAction::class, "blog.user.features.feature");
            $router->crud("$prefix/bricks/brick/{projectId:[0-9]+}", Actions\FeatureCrudAction::class, "blog.user.features.feature");
            $router->get("$prefix/bricks/brick/{projectId:[0-9]+}/archiving", Actions\FeatureCrudAction::class, "blog.user.features.feature.archiving");
            $router->post("$prefix/bricks/brick/{projectId:[0-9]+}/archiving", Actions\FeatureCrudAction::class);
            $router->get("$prefix/bricks/tags/index/{id:[0-9]+}", TagIndexAction::class, "blog.user.features.tags");
            $router->crud("$prefix/bricks/tags/{projectId:[0-9]+}", TagCrudAction::class, "blog.user.features.tags");
            $router->crud("$prefix/bricks/summary/{projectId:[0-9]+}", SummaryCrudAction::class, "blog.user.features.summary");
        }
    }

}
