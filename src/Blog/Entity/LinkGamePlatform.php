<?php

namespace App\Blog\Entity;

class LinkGamePlatform {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int 
     */
    protected $gameId;

    /**
     *
     * @var int
     */
    protected $platformId;

    /**
     * 
     * @return int
     */
    public function getgameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @return int
     */
    public function getPlatformId(): int {
        return $this->platformId;
    }

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * 
     * @param int $platformId
     */
    function setPlatformId(int $platformId) {
        $this->platformId = $platformId;
    }

}
