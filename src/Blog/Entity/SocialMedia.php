<?php

namespace App\Blog\Entity;

class SocialMedia {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $projectId;

    /**
     *
     * @var int
     */
    protected $mediaId;

    /**
     *
     * @var string
     */
    protected $address;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getProjectId(): int {
        return $this->projectId;
    }

    /**
     * 
     * @return int
     */
    function getMediaId(): int {
        return $this->mediaId;
    }

    /**
     * 
     * @return string
     */
    function getAddress(): string {
        return $this->address;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $projectId
     */
    function setProjectId(int $projectId) {
        $this->projectId = $projectId;
    }

    /**
     * 
     * @param int $mediaId
     */
    function setMediaId(int $mediaId) {
        $this->mediaId = $mediaId;
    }

    /**
     * 
     * @param string $address
     */
    function setAddress(string $address) {
        $this->address = $address;
    }

}
