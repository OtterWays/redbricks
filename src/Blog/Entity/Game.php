<?php

namespace App\Blog\Entity;

use Framework\Entity\Timestamp;

class Game {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $slug;

    /**
     *
     * @var string
     */
    protected $author;

    /**
     *
     * @var string
     */
    protected $content;

    /**
     *
     * @var string
     */
    protected $image;

    /**
     *
     * @var int
     */
    protected $published;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var int
     */
    protected $fee;

    /**
     *
     * @var int
     */
    protected $statusId;

    /**
     *
     * @var string
     */
    protected $status;

    use Timestamp;

    /**
     * 
     * @return string
     */
    public function getThumb(): string {
        ['filename' => $filename, 'extension' => $extension] = pathinfo($this->image);
        return '/uploads/' . $this->id . '/game/' . $filename . '_thumb.' . $extension;
    }

    /**
     * 
     * @return string
     */
    public function getIllustration(): string {
        ['filename' => $filename, 'extension' => $extension] = pathinfo($this->image);
        return '/uploads/' . $this->id . '/game/' . $filename . '_illustration.' . $extension;
    }

    /**
     * 
     * @return string
     */
    public function getImageUrl(): string {
        return '/uploads/' . $this->id . '/game/' . $this->image;
    }

    /**
     * 
     * @return int
     */
    public function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * 
     * @return float
     */
    function getFee(): float {
        return $this->fee / 10;
    }

    /**
     * 
     * @param int $fee
     */
    function setFee(int $fee) {
        $this->fee = $fee;
    }

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    function getSlug(): string {
        return $this->slug;
    }

    /**
     * 
     * @return string
     */
    function getAuthor(): string {
        return $this->author;
    }

    /**
     * 
     * @return string|null
     */
    function getContent(): ?string {
        return $this->content;
    }

    /**
     * 
     * @return string|null
     */
    function getImage(): ?string {
        return $this->image;
    }

    /**
     * 
     * @return int
     */
    function getPublished(): int {
        return $this->published;
    }

    /**
     * 
     * @return int|null
     */
    function getStatusId(): ?int {
        return $this->statusId;
    }

    /**
     * 
     * @return string
     */
    function getStatus(): string {
        return $this->status;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $slug
     */
    function setSlug(string $slug) {
        $this->slug = $slug;
    }

    /**
     * 
     * @param string $author
     */
    function setAuthor(string $author) {
        $this->author = $author;
    }

    /**
     * 
     * @param string $content
     */
    function setContent(string $content) {
        $this->content = $content;
    }

    /**
     * 
     * @param string|null $image
     */
    function setImage(?string $image) {
        $this->image = $image;
    }

    /**
     * 
     * @param int $published
     */
    function setPublished(int $published) {
        $this->published = $published;
    }

    /**
     * 
     * @param string $name
     */
    function setName(string $name) {
        $this->name = $name;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param int $statusId
     */
    function setStatusId(int $statusId) {
        $this->statusId = $statusId;
    }

    /**
     * 
     * @param string|null $status
     */
    function setStatus(?string $status) {
        $this->status = $status;
    }

}
