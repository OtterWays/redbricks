<?php

namespace App\Blog\Entity;

class FeatureContent {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $featureId;

    /**
     *
     * @var string
     */
    protected $content;

    /**
     *
     * @var string
     */
    protected $langcode;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string|null
     */
    function getContent(): ?string {
        return $this->content;
    }

    /**
     * 
     * @return string|null
     */
    function getLangcode(): ?string {
        return $this->langcode;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $content
     */
    function setContent(string $content) {
        $this->content = $content;
    }

    /**
     * 
     * @param string $langcode
     */
    function setLangcode(string $langcode) {
        $this->langcode = $langcode;
    }

    /**
     * 
     * @return int
     */
    function getFeatureId(): int {
        return $this->featureId;
    }

    /**
     * 
     * @param int $featureId
     */
    function setFeatureId(int $featureId) {
        $this->featureId = $featureId;
    }

}
