<?php

namespace App\Blog\Entity;

class Tag {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string|null
     */
    protected $name;

    /**
     *
     * @var string|null
     */
    protected $nameEn;

    /**
     *
     * @var string|null
     */
    protected $nameFr;

    /**
     *
     * @var string|null
     */
    protected $nameEs;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     *
     * @var int
     */
    protected $color;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string|null
     */
    function getName(): ?string {
        return $this->name;
    }

    /**
     * 
     * @return int
     */
    function getGameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @return int
     */
    function getColor(): int {
        return $this->color;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string|null $name
     */
    function setName(?string $name) {
        $this->name = $name;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * 
     * @param int $color
     */
    function setColor(int $color) {
        $this->color = $color;
    }

    /**
     * 
     * @return string|null
     */
    function getNameEn(): ?string {
        return $this->nameEn;
    }

    /**
     * 
     * @return string|null
     */
    function getNameFr(): ?string {
        return $this->nameFr;
    }

    /**
     * 
     * @return string|null
     */
    function getNameEs(): ?string {
        return $this->nameEs;
    }

    /**
     * 
     * @param string|null $nameEn
     */
    function setNameEn(?string $nameEn) {
        $this->nameEn = $nameEn;
    }

    /**
     * 
     * @param string|null $nameFr
     */
    function setNameFr(?string $nameFr) {
        $this->nameFr = $nameFr;
    }

    /**
     * 
     * @param string|null $nameEs
     */
    function setNameEs(?string $nameEs) {
        $this->nameEs = $nameEs;
    }

}
