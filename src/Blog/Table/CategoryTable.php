<?php

namespace App\Blog\Table;

use Framework\Database\Table;

class CategoryTable extends Table {

    protected $table = "category";

    public function findList(): array {
        $name = "name_" . getenv("LANG");
        $results = $this->pdo
                ->query("SELECT id, $name as name FROM {$this->table} ORDER BY slug")
                ->fetchAll(\PDO::FETCH_NUM);
        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }
        return $list;
    }

}
