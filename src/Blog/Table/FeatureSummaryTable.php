<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\FeatureSummary;
use App\Localization\Table\LangTable;
use Framework\Database\Query;

class FeatureSummaryTable extends Table {

    protected $entity = FeatureSummary::class;
    protected $table = "features_summary";

    public function findAll(): Query {
        $lang = new LangTable($this->pdo);
        return $this->makeQuery()
                        ->select("f.*, l.name as language")
                        ->join($lang->getTable() . " as l", "l.code = f.langcode");
    }

    public function find(int $id) {
        return $this->findAll()
                        ->where("f.id = $id")
                        ->fetchOrFail();
    }

    public function findAllLang(int $gameId) {
        return $this->findAll()
                        ->where("f.game_id = $gameId");
    }

    public function findShowEn(int $gameId) {
        return $this->findAll()
                        ->where("f.game_id = $gameId AND f.langcode='en'")
                        ->fetchOrFail();
    }

    public function findShow(int $gameId) {
        $langcode = getenv("LANG");
        return $this->findAll()
                        ->where("f.game_id = $gameId AND (f.langcode = '$langcode' OR (f.langcode = 'en' AND f.game_id NOT IN "
                                . "(SELECT f.game_id FROM {$this->getTable()} as f WHERE f.langcode = '$langcode')))")
                        ->fetchOrFail();
    }

}
