<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\Favorite;

class FavoriteTable extends Table {

    protected $entity = Favorite::class;
    protected $table = "favorites";

    public function findAllForUser(int $user_id) {
        return $this->findAll()
                        ->where("f.user_id = $user_id");
    }

    public function change(int $game_id, int $user_id) {
        $favorite = $this->findAll()
                ->where("f.user_id = $user_id AND f.game_id = $game_id")
                ->fetch();
        if ($favorite != false) {
            $this->delete($favorite->getId());
        } else {
            $this->insert([
                "game_id" => $game_id,
                "user_id" => $user_id
            ]);
        }
    }

}
