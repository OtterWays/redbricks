<?php

namespace App\Blog\Table;

use Framework\Database\Table;

class AttachmentTable extends Table {
    
    protected $table = "attachments";
    
    public function findByBug($bugId) {
         return $this->findAll()->where("bug_id = $bugId");
    }
    
    public function findBy(string $field, string $value) {
        return $this->makeQuery()->where("$field = :field")->params(["field" => $value]);
    }
}
