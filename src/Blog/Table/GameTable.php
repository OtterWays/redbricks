<?php

namespace App\Blog\Table;

use App\Blog\Entity\Game;
use App\Shop\Table\PurchaseTable;
use Framework\Database\Query;
use Framework\Database\Table;
use App\Auth\UserTable;
use App\Blog\Table\FavoriteTable;
use App\Comments\Table\CommentsTable;
use App\Bug\Table\BugTable;
use App\News\Table\NewsTable;
use App\Survey\Table\SurveyTable;
use App\Blog\Table\GameContentTable;
use App\Blog\Table\ContributorTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\StatusGameTable;

class GameTable extends Table {

    protected $entity = Game::class;
    protected $table = "games";

    public function find(int $id) {
        return $this->findAll()->where("g.id = $id")->fetchOrFail();
    }

    public function findEn(int $id) {
        return $this->findAllEn()->where("g.id = $id")->fetchOrFail();
    }

    public function findAll(): Query {
        $langcode = getenv("LANG");
        return $this->findAllCommon($langcode);
    }

    public function findAllEn(): Query {
        return $this->findAllCommon('en');
    }

    private function findAllCommon(string $langcode) {
        $user = new UserTable($this->pdo);
        $gameContent = new GameContentTable($this->pdo);
        $licence = new LicenceTable($this->pdo);
        $name = 'l.name_' . getenv("LANG");
        $statusTable = new StatusGameTable($this->pdo);
        $status = 's.name_' . getenv('LANG');
        return $this->makeQuery()
                        ->select("g.*, u.displayname as author, gc.content as content, $name as licenceName, $status as status")
                        ->join($user->getTable() . " as u", "u.id = g.user_id")
                        ->join($gameContent->getTable() . " as gc", "gc.game_id = g.id")
                        ->join($licence->getTable() . " as l", "l.id = g.licence")
                        ->join($statusTable->getTable() . " as s", "s.id = g.status_id")
                        ->where("gc.langcode = '$langcode' OR (gc.langcode = 'en' AND g.id NOT IN "
                                . "(SELECT gc.game_id FROM {$gameContent->getTable()} as gc WHERE gc.langcode = '$langcode'))")
                        ->order("g.created_at DESC");
    }

    public function findAllUser(int $id): Query {
        $contributor = new ContributorTable($this->pdo);
        return $this->findAllInformations()
                        ->join($contributor->getTable() . " as c", "c.game_id = g.id")
                        ->where("g.user_id = $id OR c.user_id = $id")
                        ->order("g.created_at DESC");
    }

    public function findAllFinanced(int $userId): Query {
        $purchase = new PurchaseTable($this->pdo);
        return $this->findAllInformations()
                        ->distinct()
                        ->join($purchase->getTable() . " as p", "p.game_id=g.id")
                        ->where("p.user_id = $userId")
                        ->order("g.created_at DESC");
    }

    public function findLike(string $field, string $value): Query {
        return $this->findAll()
                        ->where("$field LIKE '%" . $value . "%'");
    }

    /**
     * Retourne une query avec toutes les plateformes par jeu
     */
    public function findAllLinksPlatform(): Query {
        $linkGamePlatform = new LinkGamePlatformTable($this->pdo);
        $platform = new PlatformTable($this->pdo);
        $platformName = 'p.name_' . getenv("LANG");
        return $linkGamePlatform->makeQuery()
                        ->join($platform->getTable() . " as p", "p.id = l.platform_id")
                        ->select("l.game_id as l1_game_id, p.slug as platform_slug, $platformName as platform_name, p.fa as platform_fa");
    }

    public function findSelectLinksPlatform(int $id): Query {
        return $this->findAllLinksPlatform()
                        ->where("l1_game_id = $id");
    }

    public function findAllPlatform(): Query {
        $request = $this->findAllLinksPlatform()->__toString();
        return $this->makeQuery()
                        ->join("(" . $request . ") as p", "g.id = p.l1_game_id")
                        ->select("g.*, p.*")
                        ->order("g.created_at DESC");
    }

    public function findPublic(): Query {
        return $this->findAllInformations()
                        ->where("g.published = 1")
                        ->where("g.created_at < NOW()")
                        ->order("g.created_at DESC");
    }

    public function findPublicOrderByUpdated(): Query {
        return $this->findAllInformations()
                        ->where("g.published = 1")
                        ->where("g.created_at < NOW()")
                        ->order("g.updated_at DESC");
    }

    public function findPublicOrderByRand(): Query {
        return $this->findAllInformations()
                        ->where("g.published = 1")
                        ->where("g.created_at < NOW()")
                        ->order("RAND()");
    }

    public function findFavorite(int $user_id): Query {
        $favorite = new FavoriteTable($this->pdo);
        return $this->findAllInformations()
                        ->join($favorite->getTable() . " as f", "f.game_id = g.id")
                        ->where("f.user_id = $user_id")
                        ->order("g.created_at DESC");
    }

    public function concatArray(array $values): string {
        return join(",", array_map(function ($value) {
                    return "'" . $value . "'";
                }, $values));
    }

    public function filterPlatforms(Query $query, array $platformIds): Query {
        $values = $this->concatArray($platformIds);
        return $query->distinct()->join("links_game_platform as p", "p.game_id = g.id AND p.platform_id IN ($values)", "INNER");
    }

    public function filterLicences(Query $query, array $licenceIds): Query {
        $values = $this->concatArray($licenceIds);
        return $query
                        ->distinct()
                        ->where("g.licence IN ($values) OR g.graphics_licence IN ($values) OR g.music_licence IN ($values)");
    }

    public function findPublicForSearch(string $value): Query {
        $string = explode(",", $value);

        $values = join(",", array_map(function ($value) {
                    return "'" . $value . "'";
                }, $string));
        $values = "(" . $values . ")";

        return $this->findPublic()->where("g.id IN $values");
    }

    /**
     * 
     * @param int $gameId
     * @return false|Game
     */
    public function findShow(int $gameId) {
        return $this->findPublic()->where("g.id = $gameId")->fetch();
    }

    public function findShowPlatform(int $gameId): Query {
        $linkGamePlatform = new LinkGamePlatformTable($this->pdo);
        $platform = new PlatformTable($this->pdo);
        $platformName = "p.name_" . getenv("LANG");
        return $linkGamePlatform->makeQuery()
                        ->join($platform->getTable() . " as p", "p.id = l.platform_id")
                        ->select("l.game_id as l1_game_id, p.slug as platform_slug, $platformName as platform_name, p.fa as platform_fa")
                        ->where("l.game_id = $gameId");
    }

    public function findPublicForPlatform(array $id): Query {
        $id = implode(",", $id);
        return $this->findPublic()->where("g.id IN ($id)");
    }

    /**
     * @return Query
     */
    public function makeRequestTable(): Query {
        return (new RequestTable($this->pdo))
                        ->from($this->request_table, $this->request_table[0])
                        ->into($this->entity);
    }

    public function findAllInformations() {
        $langcode = getenv("LANG");
        $comments = new CommentsTable($this->pdo);
        $bugs = new BugTable($this->pdo);
        $news = new NewsTable($this->pdo);
        $user = new UserTable($this->pdo);
        $survey = new SurveyTable($this->pdo);
        $gameContent = new GameContentTable($this->pdo);
        $favorite = new FavoriteTable($this->pdo);
        $licence = new LicenceTable($this->pdo);
        $status = new StatusGameTable($this->pdo);
        $name = 'l.name_' . getenv("LANG");
        $nameGraphics = 'gl.name_' . getenv("LANG");
        $nameMusic = 'ml.name_' . getenv("LANG");
        $nameStatus = 's.name_' . getenv('LANG');

        return $this->makeQuery()
                        ->select("DISTINCT g.*, "
                                . "(SELECT COUNT(c.id) FROM {$comments->getTable()} as c WHERE c.game_id = g.id) as nbComments, "
                                . "(SELECT COUNT(b.id) FROM {$bugs->getTable()} as b WHERE b.project_id = g.id) as nbBugs, "
                                . "(SELECT COUNT(n.id) FROM {$news->getTable()} as n WHERE n.project_id = g.id) as nbNews, "
                                . "(SELECT COUNT(s.id) FROM {$survey->getTable()} as s WHERE s.project_id = g.id) as nbSurveys, "
                                . "(SELECT COUNT(f.id) FROM {$favorite->getTable()} as f WHERE f.game_id = g.id) as nbFavorites, "
                                . "u.displayname as author, "
                                . "gc.content as content, "
                                . "$name as licenceName, "
                                . "$nameGraphics as graphicsLicenceName, "
                                . "$nameMusic as musicLicenceName, "
                                . "$nameStatus as status")
                        ->join($user->getTable() . " as u", "u.id = g.user_id")
                        ->join($gameContent->getTable() . " as gc", "gc.game_id = g.id")
                        ->join($licence->getTable() . " as l", "l.id = g.licence")
                        ->join($licence->getTable() . " as gl", "gl.id = g.graphics_licence")
                        ->join($licence->getTable() . " as ml", "ml.id = g.music_licence")
                        ->join($status->getTable() . " as s", "s.id = g.status_id")
                        ->where("gc.langcode = '$langcode' OR (gc.langcode = 'en' AND g.id NOT IN "
                                . "(SELECT gc.game_id FROM {$gameContent->getTable()} as gc WHERE gc.langcode = '$langcode'))");
    }

}
