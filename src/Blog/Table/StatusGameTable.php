<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class StatusGameTable extends Table {

    protected $table = "status_game";

    public function findAll(): Query {
        $name = 's.name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("s.id, $name as name")
                        ->order("s.id");
    }

}
