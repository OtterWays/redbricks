<?php

use App\Shop\Action\TipsAction;
use App\Shop\Action\PaymentAction;
use App\Shop\Action\StripeAccountAction;
use App\Shop\ShopWidget;
use function DI\get;
use function DI\add;
use function DI\object;
use Framework\Api\Stripe;

return [
    'admin.widgets' => add([
        get(ShopWidget::class)
    ]),
    //ProductShowAction::class => object()->constructorParameter('stripeKey', get('stripe.key')),
    TipsAction::class => object()->constructorParameter('stripeKey', get('stripe.key')),
    PaymentAction::class => object()->constructorParameter('stripeKey', get('stripe.key')),
    StripeAccountAction::class => object()->constructorParameter('stripeKey', get('stripe.secret')),
    Stripe::class => object()->constructor(get('stripe.secret'))
];
