<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\GameTable;
use App\Shop\Table\RewardTable;
use App\Shop\Table\SubscriptionTable;

class RewardContributorsAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;
    
    /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;
    
    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, RewardTable $rewardTable, SubscriptionTable $subscriptionTable
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->rewardTable = $rewardTable;
        $this->subscriptionTable = $subscriptionTable;
    }

    public function __invoke(ServerRequestInterface $request) {
        $gameId = $request->getAttribute("projectId");
        $rewardId = $request->getAttribute("rewardId");
        $game = $this->gameTable->find($gameId);
        $reward = $this->rewardTable->find($rewardId);
        $subscribers = $this->subscriptionTable->findAllBy("reward_id", $rewardId);
        return $this->renderer->render('@shop/rewards/contributorsList', compact("game", "reward", "subscribers"));
    }

}
