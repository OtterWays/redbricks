<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use App\Blog\Table\GameTable;
use App\Shop\Table\SubscriptionTable;
use Psr\Http\Message\ServerRequestInterface;

class SubscriptionContributorsAction {
    /**
     *
     * @var RendererInterface
     */
    protected $renderer;
    
    /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, SubscriptionTable $subscriptionTable
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->subscriptionTable = $subscriptionTable;
    }

    public function __invoke(ServerRequestInterface $request) {
        $gameId = $request->getAttribute("projectId");
        $game = $this->gameTable->find($gameId);
        $subscribers = $this->subscriptionTable->findAllBy("game_id", $gameId);
        return $this->renderer->render('@shop/rewards/allContributorsList', compact("game", "reward", "subscribers"));
    }
}
