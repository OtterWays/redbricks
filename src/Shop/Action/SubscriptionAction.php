<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use App\Blog\Table\GameTable;
use App\Shop\Table\RewardTable;
use App\Shop\Table\SubscriptionTable;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Router;
use Framework\Actions\RouterAwareAction;
use Framework\Database\NoRecordException;
use Framework\Auth;

class SubscriptionAction {

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, RewardTable $rewardTable, SubscriptionTable $subscriptionTable, Router $router, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->rewardTable = $rewardTable;
        $this->subscriptionTable = $subscriptionTable;
        $this->router = $router;
        $this->auth = $auth;
    }

    public function __invoke(ServerRequestInterface $request) {
        $gameId = $request->getAttribute("gameId");
        $rewardId = $request->getAttribute("rewardId");
        $game = $this->gameTable->find($gameId);
        $rewards = $this->rewardTable->findAllBy("project_id", $game->getId());
        $user = $this->auth->getUser();
        try {
            $subscription = $this->subscriptionTable->findByUserGame($user->getId(), $gameId);
            return $this->redirect('blog.show', [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        } catch (NoRecordException $e) {
            
        }
        if ($rewardId != 0) {
            try {
                $reward = $this->rewardTable->find($rewardId);
            } catch (NoRecordException $e) {
                return $this->redirect('blog.show', [
                            "slug" => $game->getSlug(),
                            "id" => $game->getId()
                ]);
            }

            if ($reward->getContributorsMax() != 0 && $reward->getContributorsNb() >= $reward->getContributorsMax()) {
                return $this->redirect('blog.show', [
                            "slug" => $game->getSlug(),
                            "id" => $game->getId()
                ]);
            }
        }

        return $this->renderer->render('@shop/subscription', compact('game', 'reward', 'rewards'));
    }

}
