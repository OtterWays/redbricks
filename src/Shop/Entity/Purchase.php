<?php

namespace App\Shop\Entity;

class Purchase {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var int
     */
    protected $featureId;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     *
     * @var int
     */
    protected $price;

    /**
     *
     * @var int
     */
    protected $commission;

    /**
     *
     * @var string
     */
    protected $country;

    /**
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     *
     * @var string
     */
    protected $stripeId;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * @return int|null
     */
    public function getFeatureId(): ?int {
        return $this->featureId;
    }

    /**
     * @param int|null $featureId
     */
    public function setFeatureId(?int $featureId) {
        $this->featureId = $featureId;
    }

    /**
     * @return int|null
     */
    public function getGameId(): ?int {
        return $this->gameId;
    }

    /**
     * @param int $gameId
     */
    public function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price) {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getCommission(): ?int {
        return $this->commission;
    }

    /**
     * @param int $commission
     */
    public function setCommission(int $commission) {
        $this->commission = $commission;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country) {
        $this->country = $country;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|string|null $datetime
     */
    public function setCreatedAt($datetime): void {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

    /**
     * @return string|null
     */
    public function getStripeId(): ?string {
        return $this->stripeId;
    }

    /**
     * @param string|null $stripeId
     */
    public function setStripeId(?string $stripeId) {
        $this->stripeId = $stripeId;
    }

}
