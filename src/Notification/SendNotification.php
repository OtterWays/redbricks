<?php

namespace App\Notification;

use App\Notification\Table\NotificationTable;
use App\Blog\Table\GameTable;
use App\Auth\UserTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PurchaseTable;
use App\Shop\Table\PendingPurchaseTable;
use Framework\Renderer\RendererInterface;
use Framework\Auth;

class SendNotification {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var NotificationTable
     */
    protected $notificationTable;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     *
     * @var PurchaseTable
     */
    protected $purchaseTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var string
     */
    protected $from;

    /**
     *
     * @var \Swift_Mailer
     */
    protected $mailer;

    public function __construct(RendererInterface $renderer, NotificationTable $notificationTable, GameTable $gameTable, UserTable $userTable, FeatureTable $featureTable, PendingPurchaseTable $pendingPurchaseTable, PurchaseTable $purchaseTable, Auth $auth, string $from, \Swift_Mailer $mailer) {
        $this->renderer = $renderer;
        $this->notificationTable = $notificationTable;
        $this->gameTable = $gameTable;
        $this->userTable = $userTable;
        $this->featureTable = $featureTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->purchaseTable = $purchaseTable;
        $this->auth = $auth;
        $this->from = $from;
        $this->mailer = $mailer;
    }

    public function send(string $key, array $params) {
        if (!empty($params['gameId'])) {
            $game = $this->gameTable->find($params['gameId']);
        }
        if (!empty($params['userId'])) {
            $user = $this->userTable->find($params['userId']);
        } else if (!empty($params['list'])) {
            $listPlayers = $this->findPlayersNotif($params['gameId'], $key);
            $key = lcfirst(join('', array_map('ucfirst', explode('_', $key))));
        } else {
            $user = $this->userTable->find($game->getUserId());
        }

        if (!empty($params['featureId'])) {
            $feature = $this->featureTable->find($params['featureId']);
        }

        if (!empty($user)) {
            $notificationPref = $this->notificationTable->findBy("user_id", $user->getId())->$key;
        }
        if (!empty($listPlayers) || (!empty($notificationPref) && $notificationPref === "1")) {
            switch ($key) {
                case 'badge' :
                    $title = 'A new badge on RedBricks!';
                    $opts = compact('user');
                    break;
                case 'changeBrick' :
                    $title = '[brick] ' . $feature->getName();
                    $opts = compact('user', 'feature', 'game');
                    break;
                case 'changeBug' :
                    $bug = $params['bug'];
                    $title = '[bug report] ' . $bug->getTitle();
                    $opts = compact('user', 'game', 'bug');
                    break;
                case 'changeContri' :
                    if (!empty($params['bug'])) {
                        $bug = $params['bug'];
                        $title = '[RedBricks] ' . $bug->getTitle();
                        $opts = compact('user', 'game', 'bug');
                    } else {
                        $title = '[RedBricks] ' . $feature->getName();
                        $opts = compact('user', 'game', 'feature');
                    }     
                    break;
                case 'collect' :
                    $title = '[collect] ' . $feature->getName();
                    $opts = compact('user', 'feature');
                    break;
                case 'comment' :
                    $title = '[' . $game->getName() . '] A new comment';
                    $comment = $params['comment'];
                    $opts = compact('game', 'comment');
                    break;
                case 'commentContri' :
                    $comment = $params['comment'];
                    if (!empty($params['bug'])) {
                        $bug = $params['bug'];
                        $title = '[' . $bug->getTitle() . '] A new comment';
                        $opts = compact('user', 'game', 'bug', 'comment');
                    } else {
                        $title = '[' . $feature->getName() . '] A new comment';
                        $opts = compact('user', 'game', 'feature', 'comment');
                    }
                    break;
                case 'deadline' :
                    $title = 'deadline';
                    $opts = compact('user', 'game', 'feature');
                    break;
                case 'mp' :
                    $message = $params['message'];
                    $title = '[RedBricks] New private message';
                    $opts = compact('user', 'message');
                    break;
                case 'newBrick' :
                    $title = '[' . $game->getName() . '] ' . $feature->getName();
                    $opts = compact('user', 'game', 'feature');
                    break;
                case 'newBug' :
                    $bug = $params['bug'];
                    $title = '[' . $game->getName() . '] ' . $bug->getTitle();
                    $opts = compact('user', 'game', 'bug');
                    break;
                case 'news' :
                    $news = $params['news'];
                    $title = '[News] ' . $game->getName() . ' : ' . $news['title'];
                    $opts = compact('user', 'game', 'news');
                    break;
                case 'tip' :
                    $title = '[' . $game->getName() . '] New tip';
                    $opts = compact('user', 'game');
                    break;
                default :
                    $title = "";
                    $opts = compact();
                    break;
            }
            $message = new \Swift_Message($title);
            $message->setBody($this->renderer->render('@notification/email/text/' . $key, $opts));
            $message->addPart($this->renderer->render('@notification/email/html/' . $key, $opts), 'text/html');
            if (!empty($user)) {
                $message->setTo($user->getEmail());
            } else {
                $message->setBcc($listPlayers);
            }
            $message->setFrom($this->from);
            $this->mailer->send($message);
        }
    }

    private function findPlayersNotif(int $gameId, string $key): array {
        $players = $this->notificationTable->findUsers($gameId, $key)->fetchAll();            

        $arrayPlayers = [];
        foreach ($players as $player) {
            $arrayPlayers[] = $player->email;
        }
        return $arrayPlayers;
    }

}
