<?php

namespace Framework\Middleware;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;

class NotFoundMiddleware {

    /**
     * Retourne propriété erreur 404. Middleware bloquant.
     */
    public function __invoke(ServerRequestInterface $request, $next) {
        /*var_dump($request);
        die();*/
        return new Response(404, [], 'Erreur 404');
    }

}
