<?php
namespace Framework\Validator;

class ValidationError
{
    
    /**
    * @var string key
    */
    private $key;

    /**
    * @var string rule
    */
    private $rule;
    
    private $messages= [
        "required" => "The %s field is required", // "Le champ %s est requis",
        "empty" => "The %s field can't be empty", // "Le champ %s ne peut être vide",
        "slug" => "The %s field is not a valid slug", // "Le champ %s n'est pas un slug valide",
        "minLength" => "The %s field must have more than %d characters", // "Le champ %s doit contenir plus de %d caractères",
        "maxLength" => "The %s field must have less than %d characters", // "Le champ %s doit contenir moins de %d caractères",
        "betweenLength" => "The %s field must have between %d and %d characters", // "Le champ %s doit contenir entre %d et %d caractères",
        "datetime" => "The %s field must be a valid date (%s)", // "Le champ %s doit être une date valide (%s)",
        "exists" => "The %s field doesn't exist in the table %s", // "Le champ %s n'existe pas dans la table %s",
        "unique" => "The %s field must be unique", // "Le champ %s doit être unique",
        "unique2" => "The %s and %s fields must be unique", //"Les champs %s et %s doivent être uniques",
        "filetype" => "The %s field doesn't have a valid format (%s)", // "Le champ %s n'est pas au format valide (%s)",
        "uploaded" => "You must upload a file", // "Vous devez uploader un fichier",
        "email" => "This email address is not valid", //Cet email ne semble pas valide
        "confirm" => "The %s and %s fields are not the same", //Les champs %s et %s ne sont pas identiques
        "size" => "This %s is too heavy (%d Mo max)", //Cette image est trop lourde
        "minDuration" => "The %s field must be more than %d days", //Le champ %s doit être supérieure à %d jours
        "maxDuration" => "The %s field must be less than %d days", //Le champ %s doit être inférieure à %d jours
        "betweenDuration" => "The %s field must be between %d and %d days", //Le champ %s doit être comprise entre %d et %d jours
        "number" => "The %s field must be more than %d", //Le champ %s doit être supérieur à %d
        "specialCharacters" => "The %s field cannot contain special characters" //Le champ %s ne peut pas avoir de caractères spéciaux
    ];

    /**
    * @var array
    */
    private $attributes;
    
    public function __construct(string $key, string $rule, array $attributes = [])
    {
        $this->key= $key;
        $this->rule= $rule;
        $this->attributes= $attributes;
    }
    
    public function __toString()
    {
        if (!array_key_exists($this->rule, $this->messages)) {
            // return "Le champ {$this->key} ne correspond pas à la règle {$this->rule}";
            return "The field {$this->key} doesn't respect the rule {$this->rule}";
        } else {
            $params= array_merge([$this->messages[$this->rule],$this->key], $this->attributes);
            return (string)call_user_func_array("sprintf", $params);
        }
    }
}
