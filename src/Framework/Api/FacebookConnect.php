<?php

namespace Framework\Api;

use Facebook\Facebook;
use Facebook\FacebookSession;

class FacebookConnect {

    private $appId;
    private $appSecret;

    /**
     * @param $appId Facebook Application ID
     * @param $appSecret Facebook Application secret
     */
    function __construct($appId, $appSecret) {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    /**
     * 
     * @return string|Facebook\GraphUser Login URL or GraphUser
     */
    function connect() {

        $fb = new Facebook([
            "app_id" => $this->appId,
            "app_secret" => $this->appSecret,
            "default_graph_version" => 'v2.10'
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
            $request = $fb->get('/me?fields=name, email', $accessToken);
            $profile = $request->getGraphUser();
            /*if ($profile->getEmail() === null) {
                throw new \Exception('L\'email n\'est pas disponible');
            }*/
            return $profile;
        } catch (\Exception $e) {
            exit();
        }       
        
    }

    /**
     * 
     * @param string $redirect_url
     * @return string
     */
    function login(string $redirect_url) {
        $fb = new Facebook([
            "app_id" => $this->appId,
            "app_secret" => $this->appSecret,
            "default_graph_version" => 'v2.10'
        ]);

        $helper = $fb->getRedirectLoginHelper();
        return $helper->getLoginUrl($redirect_url, ['email']);
    }

}
