<?php

namespace Framework\Actions;

use Framework\Database\Hydrator;
use Framework\Database\Table;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface;
use Framework\Validator;

class CrudAction {

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var mixed
     */
    protected $table;

    /**
     * @var FlashService
     */
    protected $flash;

    /**
     * @var string
     */
    protected $viewPath;

    /**
     * @var string
     */
    protected $routePrefix;

    /**
     * @var string
     */
    protected $messages;

    /**
     * @var array
     */
    protected $acceptedParams = [];

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, Table $table, FlashService $flash
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->table = $table;
        $this->flash = $flash;
        $this->messages = [
            "create" => _("The game has been created. A moderator must to approve it to publishing."),
            "edit" => _("The game has been modified.")
        ];
    }

    public function __invoke(Request $request) {
        //var_dump($request);die();
        $this->renderer->addGlobal("viewPath", $this->viewPath);
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        if ($request->getMethod() === "DELETE") {
            return $this->delete($request);
        }

        if (substr((string) $request->getUri(), -3) === "new") {
            return $this->create($request);
        }
        if ($request->getAttribute("id") != null or $request->getAttribute("featureId")) {
            return $this->edit($request);
        }
        return $this->index($request);
    }

    /**
     * Affiche la liste des éléments
     * @param Request $request
     * @return string
     */
    protected function index(Request $request): string {
        $params = $request->getQueryParams();
        //pagination réglée à 12
        $items = $this->table->findAll()->paginate(12, $params["p"] ?? 1);
        return $this->renderer->render($this->viewPath . "/index", compact("items"));
    }

    /**
     * Edite un élément
     * @param Request $request
     * @return ResponseInterface|string
     */
    public function edit(Request $request) {
        $id = (int) $request->getAttribute('id');
        $item = $this->table->find($id);

        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                //var_dump( $request);die();
                $this->table->update($item->getId(), $this->prePersist($request, $item));
                $this->postPersist($request, $item);
                $this->flash->success($this->messages["edit"]);
                //var_dump( $this->prePersist($request, $item));die();
                return $this->redirect($this->routePrefix . ".index");
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }

        return $this->renderer->render(
                        $this->viewPath . "/edit", $this->formParams(compact("item", "errors"))
        );
    }

    /**
     * Crée un nouvel élément
     * @param Request $request
     * @return ResponseInterface|string
     */
    public function create(Request $request) {
        $item = $this->getNewEntity();
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $this->table->insert($this->prePersist($request, $item));
                $this->postPersist($request, $item);
                $this->flash->success($this->messages["create"]);
                return $this->redirect($this->routePrefix . ".index");
            }
            Hydrator::hydrate($request->getParsedBody(), $item);
            $errors = $validator->getErrors();
        }

        return $this->renderer->render(
                        $this->viewPath . "/create", $this->formParams(compact("item", "errors"))
        );
    }

    /**
     * Action de suppression
     *
     * @param Request $request
     * @return ResponseInterface
     */
    public function delete(Request $request) {
        $this->table->delete($request->getAttribute("id"));
        return $this->redirect($this->routePrefix . ".index");
    }

    /**
     * Filtre les paramètres reçu par la requête
     *
     * @param Request $request
     * @return array
     */
    protected function prePersist(Request $request, $item): array {
        return array_filter(array_merge($request->getParsedBody(), $request->getUploadedFiles()), function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Permet d'effectuer un traitement après la persistence
     * @param Request $request
     * @param $item
     */
    protected function postPersist(Request $request, $item): void {
        
    }

    /**
     * Génère le validateur pour valider les données
     *
     * @param Request $request
     * @return Validator
     */
    protected function getValidator(Request $request) {
        return new Validator(array_merge($request->getParsedBody(), $request->getUploadedFiles()));
    }

    /**
     * Génère une nouvelle entité pour l'action de création
     *
     * @return \stdClass
     */
    protected function getNewEntity() {
        $entity = $this->table->getEntity();
        return new $entity();
    }

    /**
     * Permet de traiter les paramètres à envoyer à la vue
     *
     * @param $params
     * @return array
     */
    protected function formParams(array $params): array {
        return $params;
    }

}
