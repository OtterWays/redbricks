<?php

namespace App\Localization;

use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use App\Localization\Actions\LocalizationAction;
use Framework\Auth\LoggedInMiddleware;
use App\Localization\Actions\LocalizationIndexAction;
use App\Localization\Actions\LocalizationCrudAction;
use App\Localization\Actions\MigrationFeature;
use App\Localization\Actions\MigrationGame;
use App\Localization\Actions\LocalizationFeatureCrudAction;

class LocalizationModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('localization', __DIR__ . "/views");
        $router->get('/localization/{langcode:en|fr|es}', LocalizationAction::class, 'localization.language');
        $router->get('/translations/index/{id:[0-9]+}', [LoggedInMiddleware::class, LocalizationIndexAction::class], 'localization.translation');
        $router->crud('/translations/{projectId:[0-9]+}', [LoggedInMiddleware::class, LocalizationCrudAction::class], 'localization.translation');
        $router->crud('/translations/{projectId:[0-9]+}/features/{featureId:[0-9]+}', [LoggedInMiddleware::class, LocalizationFeatureCrudAction::class], 'localization.translation.feature');
    }

}
