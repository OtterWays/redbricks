<?php

namespace App\Localization\Actions;

use App\Blog\Table\GameContentTable;
use App\Blog\Table\GameTable;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Localization\Table\LangTable;
use App\Blog\Table\ContributorTable;
use App\Blog\Table\FeatureContentTable;
use Framework\Session\SessionInterface;
use Framework\Auth;

class LocalizationIndexAction {

    /**
     *
     * @var LangTable
     */
    protected $langTable;

    /**
     *
     * @var GameContentTable
     */
    protected $gameContentTable;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var FeatureContentTable
     */
    protected $featureContentTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    public function __construct(
            RendererInterface $renderer, GameContentTable $gameContentTable, GameTable $gameTable, LangTable $langTable, FeatureContentTable $featureContentTable, ContributorTable $contributorTable, SessionInterface $session
    ) {
        $this->renderer = $renderer;
        $this->gameContentTable = $gameContentTable;
        $this->gameTable = $gameTable;
        $this->langTable = $langTable;
        $this->featureContentTable = $featureContentTable;
        $this->contributorTable = $contributorTable;
        $this->session = $session;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $translations = $this->gameContentTable->findAllByGame($id);
        $game = $this->gameTable->find($id);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $features = $this->featureContentTable->findAllByGame($id);
        $languages = $this->featureContentTable->findAllByGameWithUnusedLanguage($id);
        foreach ($languages as $language) {
            $langs[$language->getFeatureId()] = $language->nbLang;
        }
        //echo '<pre>';        var_dump($langs); echo '</pre>'; die();
        $nbLang = $this->langTable->unusedLanguage($id)->count();
        $nbLangTot = $this->langTable->findAll()->count();
        return $this->renderer->render("@localization/index", compact("translations", "game", "nbLang", "features", "langs", "nbLangTot"));
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
