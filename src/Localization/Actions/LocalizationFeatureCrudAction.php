<?php

namespace App\Localization\Actions;

use Framework\Actions\CrudAction;
use Framework\Session\FlashService;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\FeatureContentTable;
use App\Localization\Table\LangTable;
use App\Blog\Table\FeatureTable;
use Framework\Session\SessionInterface;
use Framework\Auth;
use App\Blog\Table\GameTable;
use App\Blog\Table\ContributorTable;

class LocalizationFeatureCrudAction extends CrudAction {

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var LangTable
     */
    protected $langTable;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var ContributorTable
     */
    protected $ContributorTable;
    protected $acceptedParams = ["feature_id", "name", "content", "langcode"];

    public function __construct(
            RendererInterface $renderer, Router $router, FeatureContentTable $table, FlashService $flash, LangTable $langTable, FeatureTable $featureTable, SessionInterface $session, GameTable $gameTable, ContributorTable $contributorTable) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->langTable = $langTable;
        $this->featureTable = $featureTable;
        $this->session = $session;
        $this->gameTable = $gameTable;
        $this->ContributorTable = $contributorTable;
        $this->messages = [
            "create" => _("A translation has been created."),
            "edit" => _("The translation has been modified.")
        ];
    }

    public function edit(ServerRequestInterface $request) {
        $item = $this->table->find($request->getAttribute("id"));
        $feature = $this->featureTable->find($item->getFeatureId());
        $game = $this->gameTable->find($feature->getProjectId());
        $this->validateAccessRights($feature->getUserId(), $game->getUserId(), $game->getId());
        $gameId = $request->getAttribute("projectId");
        $item->gameId = $gameId;
        $languages = $this->langTable->findAll();
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $fields = $this->prePersist($request, $item);
                //echo '<pre>'; var_dump($item, $fields); echo '</pre>'; die();
                $this->table->update($item->getId(), $fields);
                $this->flash->success($this->messages["edit"]);
                $this->postPersist($request, $item);
                return $this->redirect("localization.translation", [
                            "id" => $gameId
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }
        return $this->renderer->render('@localization/edit', $this->formParams(compact("item", "errors", "languages")));
    }

    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $item->setFeatureId($request->getAttribute('featureId'));
        $gameId = $request->getAttribute("projectId");
        $item->gameId = $gameId;
        $feature = $this->featureTable->find($item->getFeatureId());
        $game = $this->gameTable->find($feature->getProjectId());
        $this->validateAccessRights($feature->getUserId(), $game->getUserId(), $game->getId());
        $item->name = $feature->getName();
        $item->setContent($feature->getContent());
        $languages = $this->langTable->unusedLanguageForFeature($item->getFeatureId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $this->table->insert($this->prePersist($request, $item));
                $this->flash->success($this->messages["create"]);
                $this->postPersist($request, $item);
                return $this->redirect("localization.translation", [
                            "id" => $gameId
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }
        return $this->renderer->render('@localization/create', $this->formParams(compact("item", "errors", "languages")));
    }

    public function prePersist(ServerRequestInterface $request, $item): array {
        $params = $request->getParsedBody();
        if (!empty($request->getAttribute("featureId"))) {
            $params["feature_id"] = $request->getAttribute("featureId");
        }

        //echo '<pre>';        var_dump($params, $this->acceptedParams); echo '</pre>'; die();
        return array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function delete(ServerRequestInterface $request) {
        $feature = $this->featureTable->find($request->getAttribute("id"));
        $game = $this->gameTable->find($feature->getProjectId());
        $this->validateAccessRights($feature->getUserId(), $game->getUserId(), $game->getId());
        $this->table->delete($request->getAttribute("id"));
        $gameId = $request->getAttribute('projectId');
        return $this->redirect("localization.translation", [
                    "id" => $gameId
        ]);
    }

    public function formParams($params): array {
        $params['feature'] = true;
        return $params;
    }

    protected function validateAccessRights(int $featureUserId, int $gameUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null ||
                ($role == 'user' && $userId != $featureUserId && $userId != $gameUserId && !$this->ContributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
