<?php

namespace App\Discussion\Action;

use App\Auth\UserTable;
use App\Discussion\Table\MessageTable;
use App\Discussion\Table\ThreadTable;
use App\Discussion\Table\ThreadUnreadTable;
use Framework\Auth;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;
use App\Discussion\Entity\Message;
use App\Notification\SendNotification;

class MessageViewAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     * @var MessageTable
     */
    private $messageTable;

    /**
     * @var ThreadTable
     */
    private $threadTable;

    /**
     *
     * @var ThreadUnreadTable
     */
    protected $threadUnreadTable;

    /**
     * @var Auth
     */
    private $auth;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    public function __construct(
            RendererInterface $renderer,
            FlashService $flashService,
            UserTable $userTable,
            MessageTable $messageTable,
            ThreadTable $threadTable,
            ThreadUnreadTable $threadUnreadTable,
            SendNotification $sendNotification,
            Auth $auth) {
        $this->renderer = $renderer;
        $this->flashService = $flashService;
        $this->userTable = $userTable;
        $this->messageTable = $messageTable;
        $this->threadTable = $threadTable;
        $this->threadUnreadTable = $threadUnreadTable;
        $this->sendNotification = $sendNotification;
        $this->auth = $auth;
    }

    public function __invoke(ServerRequestInterface $request) {
        $user = $this->auth->getUser();
        $threads = $this->threadTable->findAllUser($user->getId());
        if (!$threads->fetch()) {
            $noThread = true;
            return $this->renderer->render('@discussion/messages', compact('noThread'));
        }
        if ($request->getMethod() === 'GET') {
            $idThread = $request->getAttribute('idThread');
            try {
                if ($idThread == 0) {
                    $idThread = $threads->fetch()->getId();
                } elseif (!in_array($user->getId(), explode(',', $this->threadTable->find($idThread)->getIdUsers()))) {
                    $idThread = $threads->fetch()->getId();
                    $this->flashService->error(_("You can't read this thread."));
                }
            } catch (NoRecordException $exception) {
                $idThread = $threads->fetch()->getId();
                $this->flashService->error(_("You can't read this thread."));
            }
        } else {
            $params = $request->getParsedBody();
            $idThread = $params['idThread'];
            if (isset($params['unread'])) {
                $this->threadUnreadTable->insertUnique([
                    "thread_id" => $idThread,
                    "user_id" => $user->getId()
                ]);
            } else {
                $validator = (new Validator($params))
                        ->required('message')
                        ->notEmpty('message');
                if ($validator->isValid()) {
                    $messageParams = [
                        'id_thread' => $idThread,
                        'id_from' => $user->getId(),
                        'message' => $params['message'],
                        'created_at' => date("Y-m-d H:i:s")
                    ];
                    try {
                        $userIds = explode(',', $this->threadTable->find($idThread)->getIdUsers());
                        if (!in_array($user->getId(), $userIds)) {
                            $idThread = $threads->fetch()->getId();
                            $this->flashService->error(_("You can't send the message in this thread."));
                        } else {
                            $this->messageTable->insert($messageParams);
                            $last_message = $this->messageTable->getPdo()->lastInsertId();
                            $this->threadTable->update($idThread, ['last_message' => $last_message]);
                            $message = new Message();
                            $message->setId($last_message);
                            $message->setMessage($params['message']);
                            $message->author = $user->getUsername();
                            $message->setIdThread($idThread);
                            foreach ($userIds as $userId) {
                                if ($userId != $user->getId()) {
                                    $this->threadUnreadTable->insertUnique([
                                        "thread_id" => $idThread,
                                        "user_id" => $userId
                                    ]);
                                    $this->sendNotification->send("mp", [
                                        "message" => $message,
                                        "userId" => $userId
                                    ]);
                                }
                            }
                            $this->flashService->success(_('Message send'));
                        }
                    } catch (NoRecordException $exception) {
                        $idThread = $threads->fetch()->getId();
                        $this->flashService->error(_("You can't send the message in this thread."));
                    }
                } else {
                    $this->flashService->error(_("Your message doesn't send"));
                    $errors = $validator->getErrors();
                }
            }
        }

        $messages = $this->messageTable->findAllThread($idThread)->fetchAll();
        $threadsUnread = $this->threadUnreadTable->findbyUser($user->getId());
        if (!isset($params['unread'])) {
            $this->threadUnreadTable->deleteByThread($idThread, $user->getId());
        }

        $users = array();
        foreach ($threads as $thread) {
            $idUsers = explode(',', $thread->getIdUsers());
            foreach ($idUsers as $idUser) {
                if ($idUser != $user->getId()) {
                    try {
                        $users[$thread->getId()][] = $this->userTable->find($idUser);
                    } catch (NoRecordException $exception) {
                        $errors = _("One or more recipients don't exist");
                    }
                }
            }
        }

        if (isset($errors) && $errors != null) {
            return $this->renderer->render('@discussion/messages',
                            compact('errors', 'user', 'threads', 'messages', 'idThread', 'users', 'threadsUnread'));
        } else {
            return $this->renderer->render('@discussion/messages',
                            compact('user', 'threads', 'messages', 'idThread', 'users', 'threadsUnread'));
        }
    }

}
