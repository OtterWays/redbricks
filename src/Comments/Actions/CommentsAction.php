<?php

namespace App\Comments\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Actions\RouterAwareAction;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\RouterInterface;
use Framework\Router;
use App\Blog\Table\GameTable;
use App\Comments\Table\CommentsTable;
use Framework\Session\SessionInterface;
use Framework\Validator;
use Framework\Session\FlashService;
use App\Blog\Table\FeatureTable;
use App\Bug\Table\BugTable;
use App\News\Table\NewsTable;
use App\Auth\UserTable;
use App\Notification\SendNotification;
use Framework\Database\NoRecordException;

class CommentsAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var BugTable
     */
    protected $bugTable;

    /**
     *
     * @var NewsTable
     */
    protected $newsTable;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    /**
     *
     * @var FlashService
     */
    protected $flash;

    /**
     * @var string
     */
    protected $from;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, CommentsTable $commentsTable, FeatureTable $featureTable, BugTable $bugTable, NewsTable $newsTable, UserTable $userTable, SendNotification $sendNotification, SessionInterface $session, FlashService $flash, string $from, \Swift_Mailer $mailer
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->commentsTable = $commentsTable;
        $this->featureTable = $featureTable;
        $this->bugTable = $bugTable;
        $this->newsTable = $newsTable;
        $this->userTable = $userTable;
        $this->sendNotification = $sendNotification;
        $this->session = $session;
        $this->flash = $flash;
        $this->from = $from;
        $this->mailer = $mailer;
        $this->messages = [
            "add" => _("Your comment has been posted."),
            "error" => _("Error. Please, check the fields."),
            "delete" => _("Deleted comment.")
        ];
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $gameId = $request->getAttribute("gameId");
        $featureId = $request->getAttribute("featureId");
        $bugId = $request->getAttribute("bugId");
        $newsId = $request->getAttribute("newsId");
        $game = $this->gameTable->findShow($gameId);
        if ($request->getMethod() === "POST") {
            $params = $request->getParsedBody();
            if (!empty($params['add'])) {
                $commentId = $this->add($params, $gameId, $featureId, $bugId, $newsId);
                $this->gameTable->update($game->getId(), [
                    "updated_at" => date("Y-m-d H:i:s")
                ]);
                if ($commentId != null) {
                    $comment = $this->commentsTable->find($commentId);
                    $this->sendNotification->send('comment', [
                        "gameId" => $gameId,
                        "comment" => $comment,
                        "userId" => $game->getUserId()
                    ]);
                    if ($featureId != 0) {
                        $feature = $this->featureTable->find($featureId);
                        if ($feature->getUserId() != $game->getUserId()) {
                            $this->sendNotification->send('commentContri', [
                                "gameId" => $gameId,
                                "featureId" => $featureId,
                                "userId" => $feature->getUserId(),
                                "comment" => $comment
                            ]);
                        }
                    } else if ($bugId != 0) {
                        $bug = $this->bugTable->find($bugId);
                        if ($bug->getUserId() != $game->getUserId()) {
                            $this->sendNotification->send('commentContri', [
                                "gameId" => $gameId,
                                "bug" => $bug,
                                "userId" => $bug->getUserId(),
                                "comment" => $comment
                            ]);
                        }
                    }
                }
            } elseif (!empty($params['delete'])) {
                $this->delete($params);
            }
        }

        if ($featureId != 0) {
            $feature = $this->featureTable->findOneShow($featureId);
            return $this->redirect("blog.feature.showOneFeature", [
                        "slug" => $feature->getSlug(),
                        "id" => $feature->getId()
            ]);
        } elseif ($bugId != 0) {
            return $this->redirect("bug", [
                        "id" => $bugId
            ]);
        } elseif ($newsId != 0) {
            return $this->redirect("news.show", [
                        "id" => $newsId
            ]);
        }
        return $this->redirect('comments.list', [
                    "slug" => $game->getSlug(),
                    "id" => $gameId
        ]);
    }

    private function add($params, $gameId, $featureId, $bugId, $newsId) {
        $validator = (new Validator($params))
                ->required('content', 'parentId')
                ->length('content', 2);
        if (is_null($this->session->get('auth.role'))) {
            $validator
                    ->required('username', 'email')
                    ->length('username', 3)
                    ->email('email');
        }
        if ($validator->isValid()) {
            if (is_null($this->session->get('auth.role'))) {
                try {
                    $user = $this->userTable->findBy("username", $params['username']);
                    if ($user->isActive()) {
                        $this->flash->error(_("This username is already used by an active account. Please, log in you to post this comment."));
                        return null;
                    } elseif ($user->getEmail() != $params['email']) {
                        $this->flash->error(_("This username is already used by an account with an other email address. Please, change your username or email."));
                        return null;
                    }
                    $userId = $user->getId();
                } catch (NoRecordException $exception) {
                    try {
                        $user = $this->userTable->findBy('email', $params['email']);
                        if ($user->isActive()) {
                            $this->flash->error(_("This email address is already used by an active account. Please, log in you to post this comment."));
                            return null;
                        } else {
                            $this->flash->error(_("This email address is already used by an account with an other username. Please, change your username or email."));
                            return null;
                        }
                    } catch (NoRecordException $e) {
                        $params['password'] = $this->randomString(8);
                        $params['activation_key'] = md5(microtime(TRUE) * 100000);
                        $this->userTable->insert([
                            "username" => $params['username'],
                            "email" => $params['email'],
                            "password" => password_hash($params['password'], PASSWORD_DEFAULT),
                            "displayname" => $params['username'],
                            "role" => "user",
                            "activation_key" => $params['activation_key'],
                            "active" => 0,
                            'created_at' => (new \DateTime())->format("Y-m-d H:i:s")
                        ]);
                        $userId = $this->userTable->getPdo()->lastInsertId();
                        $message = new \Swift_Message('Your register on RedBricks');
                        $message->setBody($this->renderer->render('@account/email/signupComment.text', $params));
                        $message->addPart($this->renderer->render('@account/email/signupComment.html', $params), 'text/html');
                        $message->setTo($params['email']);
                        $message->setFrom($this->from);
                        $this->mailer->send($message);
                    }
                }
            } else {
                $userId = $this->session->get('auth.user');
            }
            if ($params['parentId'] != 0) {
                $comment = $this->commentsTable->find($params['parentId']);
                $featureId = $comment->getFeatureId();
                $bugId = $comment->getBugId();
                $newsId = $comment->getNewsId();
            }
            $this->commentsTable->insert([
                "parent_id" => $params['parentId'],
                "user_id" => $userId,
                "content" => $params['content'],
                "game_id" => $gameId,
                "feature_id" => $featureId,
                "bug_id" => $bugId,
                "news_id" => $newsId,
                "created_at" => date("Y-m-d H:i:s")
            ]);
            $this->flash->success($this->messages["add"]);
            return $this->commentsTable->getPdo()->lastInsertId();
        } else {
            $errors = $validator->getErrors();
            $this->flash->error($this->messages["error"]);
            return null;
        }
    }

    private function delete($params) {
        $id = $params['id'];
        $comment = $this->commentsTable->find($id);
        $this->commentsTable->delete($id);
        $this->commentsTable->updateByParent($id, ["parent_id" => $comment->getParentId()]);
        $this->flash->success($this->messages["delete"]);
    }

    private function randomString(int $lenght, ?string $list = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $string = '';
        $max = mb_strlen($list, '8bit') - 1;
        for ($i = 0; $i < $lenght; ++$i) {
            $string .= $list[random_int(0, $max)];
        }
        return $string;
    }

}
