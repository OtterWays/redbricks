<?php

namespace App\Comments;

use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use App\Comments\Actions\CommentsAction;
use App\Comments\Actions\CommentListAction;

class CommentsModule extends Module {

    const DEFINITIONS = __DIR__ . '/definitions.php';

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('comments', __DIR__ . "/views");
        $router->get('/comments/{gameId:[0-9]+}/{featureId:[0-9]+}/{bugId:[0-9]+}/{newsId:[0-9]+}', CommentsAction::class, 'comments');
        $router->post('/comments/{gameId:[0-9]+}/{featureId:[0-9]+}/{bugId:[0-9]+}/{newsId:[0-9]+}', CommentsAction::class);
        $router->get("/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/comments", CommentListAction::class, "comments.list");
    }

}
