<?php

namespace App\Badge\Actions;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Badge\Table\BadgeUsersTable;
use Framework\Auth;

class BadgeAction {

    /**
     *
     * @var Renderer
     */
    protected $renderer;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var BadgeUsersTable
     */
    protected $badgeUsersTable;
    
    public function __construct(
            RendererInterface $renderer, Auth $auth, BadgeUsersTable $badgeUsersTable
    ) {

        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->badgeUsersTable = $badgeUsersTable;
    }

    public function __invoke(Request $request) {
        $user = $this->auth->getUser();
        $badges = $this->badgeUsersTable->findForUser($user->getId());

        return $this->renderer->render("@badge/badges", compact('badges'));
    }

}
