<?php

namespace App\Badge\Actions;

use Framework\Renderer\RendererInterface;
use App\Badge\Table\BadgeUsersTable;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Auth\UserTable;
use App\Blog\Table\GameTable;

class AddBadgeAction {

    /**
     *
     * @var Renderer
     */
    protected $renderer;

    /**
     *
     * @var BadgeUsersTable
     */
    protected $badgeUsersTable;
    protected $userTable;
    protected $gameTable;

    public function __construct(
            RendererInterface $renderer, BadgeUsersTable $badgeUsersTable, UserTable $userTable, GameTable $gameTable
    ) {

        $this->renderer = $renderer;
        $this->badgeUsersTable = $badgeUsersTable;
        $this->userTable = $userTable;
        $this->gameTable = $gameTable;
    }

    public function __invoke(Request $request) {
        $users = $this->userTable->findAll();
        foreach ($users as $user) {
            $this->badgeUsersTable->insertUnique([
                "badge_id" => 1,
                "user_id" => $user->getId()
            ]);
        }
        $games = $this->gameTable->findAll();
        foreach ($games as $game) {
            $this->badgeUsersTable->insertUnique([
                "badge_id" => 2,
                "user_id" => $game->getUserId()
            ]);
        }

        return $this->renderer->render("@badge/badges", compact('badges'));
    }

}
