<?php

namespace App\News\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\News\Table\NewsTable;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\GameTable;
use App\Comments\Table\CommentsTable;

class NewsShowAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;
    
    /**
     *
     * @var NewsTable
     */
    protected $newsTable;
    
     /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    public function __construct(
    RendererInterface $renderer, Router $router, NewsTable $newsTable, GameTable $gameTable, CommentsTable $commentsTable) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->newsTable = $newsTable;
        $this->gameTable = $gameTable;
        $this->commentsTable = $commentsTable;
    }

    public function __invoke(ServerRequestInterface $request) {
        $newsId = $request->getAttribute("id");
        $news = $this->newsTable->find($newsId);
        $game = $this->gameTable->find($news->getProjectId());
        $comments = $this->commentsTable->findListWithChildrenByNews($newsId);

        return $this->renderer->render("@news/show", compact("news", "game", "comments"));
    }

}
