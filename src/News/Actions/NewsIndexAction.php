<?php

namespace App\News\Actions;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Blog\Table\GameTable;
use App\News\Table\NewsTable;
use App\Blog\Table\ContributorTable;
use Framework\Session\SessionInterface;
use Framework\Auth;

class NewsIndexAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var NewsTable
     */
    protected $newsTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, NewsTable $newsTable, ContributorTable $contributorTable, SessionInterface $session
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->newsTable = $newsTable;
        $this->contributorTable = $contributorTable;
        $this->session = $session;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $game = $this->gameTable->findShow($id);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $news = $this->newsTable->findShow($id);
        return $this->renderer->render("@news/index", compact("game", "news"));
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
