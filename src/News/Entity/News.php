<?php

namespace App\News\Entity;

use Framework\Entity\Timestamp;

class News {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $projectId;

    /**
     *
     * @var string
     */
    protected $title;

    /**
     *
     * @var string
     */
    protected $content;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var string|null
     */
    protected $image;

    use Timestamp;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getProjectId(): int {
        return $this->projectId;
    }

    /**
     * 
     * @return string
     */
    function getTitle(): ?string {
        return $this->title;
    }

    /**
     * 
     * @return string
     */
    function getContent(): ?string {
        return $this->content;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return string|null
     */
    function getImage(): ?string {
        return $this->image;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $projectId
     */
    function setProjectId(int $projectId) {
        $this->projectId = $projectId;
    }

    /**
     * 
     * @param string $title
     */
    function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * 
     * @param string $content
     */
    function setContent(string $content) {
        $this->content = $content;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param string|null $image
     */
    function setImage(?string $image) {
        $this->image = $image;
    }

}
