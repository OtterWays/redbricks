<?php

namespace App\News\Table;

use Framework\Database\Table;
use App\News\Entity\News;
use App\Auth\UserTable;
use App\Blog\Table\GameTable;
use Framework\Database\Query;

class NewsTable extends Table {

    protected $entity = News::class;
    protected $table = "news";

    /**
     * 
     * @return Query
     */
    public function findAll(): Query {
        $user = new UserTable($this->pdo);
        $game = new GameTable($this->pdo);
        return $this->makeQuery()
                        ->select("n.*, u.displayname as author, g.slug as gameSlug")
                        ->join($user->getTable() . " as u", "u.id = n.user_id")
                        ->join($game->getTable() . " as g", "g.id = n.project_id")
                        ->order("n.created_at Desc");
    }

    /**
     * 
     * @param int $gameId
     * @return Query
     */
    public function findShow(int $gameId): Query {
        return $this->findAll()
                        ->where("n.project_id = $gameId");
    }

    /**
     * 
     * @param int $id
     * @return bool|mixed
     */
    public function find(int $id) {
        return $this->findAll()->where("n.id = $id")->fetchOrFail();
    }

}
