<?php

namespace App\Suggestion\Table;

use Framework\Database\Table;
use App\Suggestion\Entity\Suggestion;
use App\Suggestion\Table\VotesSuggestionTable;
use Framework\Database\Query;
use Framework\Database\QueryResult;

class SuggestionTable extends Table {

    protected $entity = Suggestion::class;
    protected $table = "suggestion";

    /**
     * 
     * @param string $order
     * @return Query
     */
    public function findAllWithOrder(string $order): Query {
        $votesSuggestion = new VotesSuggestionTable($this->pdo);
        return $this->makeQuery()
                        ->select("s.*, COUNT(v.id) as votes")
                        ->join($votesSuggestion->getTable() . " as v", "s.id = v.suggestion_id")
                        ->groupBy("s.game, s.studio")
                        ->order($order);
    }

    /**
     * 
     * @param int $userId
     * @param string $order
     * @return QueryResult
     */
    public function findAllForUser(int $userId, string $order): QueryResult {
        $suggestions = $this->findAllWithOrder($order)->fetchAll();
        $voteTable = new VotesSuggestionTable($this->pdo);
        $votesUser = $voteTable->findByUser($userId);

        foreach ($votesUser as $vote) {
            $votes[$vote->getSuggestionId()] = $vote;
        }

        foreach ($suggestions as $suggestion) {
            if (isset($votes[$suggestion->getId()])) {
                $suggestion->user = true;
            }
        }

        return $suggestions;
    }

    /**
     * 
     * @param string $search
     * @param string $order
     * @return Query
     */
    public function findWithSearch(string $search, string $order): Query {
        return $this->findAllWithOrder($order)
                        ->where("s.game LIKE '%" . $search . "%' OR s.studio LIKE '%" . $search . "%'");
    }

    /**
     * 
     * @param string $search
     * @param int $userId
     * @param string $order
     * @return QueryResults
     */
    public function findWithSearchForUser(string $search, int $userId, string $order): QueryResult {
        $suggestions = $this->findWithSearch($search, $order)->fetchAll();
        $voteTable = new VotesSuggestionTable($this->pdo);
        $votesUser = $voteTable->findByUser($userId);

        foreach ($votesUser as $vote) {
            $votes[$vote->getSuggestionId()] = $vote;
        }

        foreach ($suggestions as $suggestion) {
            if (isset($votes[$suggestion->getId()])) {
                $suggestion->user = true;
            }
        }

        return $suggestions;
    }

}
