<?php

namespace App\Support\Table;

use Framework\Database\Table;
use App\Support\Entity\Article;
use App\Support\Table\SectionTable;
use Framework\Database\Query;

class ArticleTable extends Table {

    protected $entity = Article::class;
    protected $table = "support_articles";

    /**
     * 
     * @return Query
     */
    public function findAll(): Query {
        $section = new SectionTable($this->pdo);
        return $this->makeQuery()
                        ->select("s.*, sec.name_fr as sectionNameFr, sec.slug_fr as sectionSlugFr")
                        ->join($section->getTable() . " as sec", "sec.id = s.section_id");
    }

    /**
     * 
     * @param int $sectionId
     * @return Query
     */
    public function findForSection(int $sectionId): Query {
        return $this->findAll()
                        ->where("s.section_id = $sectionId");
    }

    /**
     * 
     * @param int $id
     * @return bool|mixed
     */
    public function find(int $id) {
        return $this->findAll()
                        ->where("s.id = $id")
                        ->fetchOrFail();
    }

}
