<?php

namespace App\Bug\Actions;

use Framework\Actions\CrudAction;
use Framework\Session\FlashService;
use App\Bug\Table\BugTable;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\GameTable;
use App\Bug\Table\StatusBugTable;
use Framework\Session\SessionInterface;
use App\Bug\Entity\Bug;
use Framework\Database\Hydrator;
use App\Blog\FileUpload;
use App\Blog\Table\AttachmentTable;
use App\Bug\Table\FrequencyTable;
use App\Bug\Table\SeverityTable;
use App\Blog\Table\CategoryTable;
use App\Comments\Table\CommentsTable;
use App\Blog\Table\ContributorTable;
use App\Notification\SendNotification;
use Framework\Auth;

class BugCrudAction extends CrudAction {

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var StatusBugTable
     */
    protected $statusBugTable;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     *
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     *
     * @var AttachmentTable
     */
    protected $attachmentTable;

    /**
     *
     * @var SeverityTable
     */
    protected $severityTable;

    /**
     *
     * @var CategoryTable
     */
    protected $categoryTable;

    /**
     *
     * @var FrequencyTable
     */
    protected $frequencyTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;
    protected $acceptedParams = ["title", "project_id", "status_id", "severity_id", "category_id", "frequency_id", "description"];

    /**
     * 
     * @param RendererInterface $renderer
     * @param Router $router
     * @param BugTable $table
     * @param FlashService $flash
     * @param GameTable $gameTable
     */
    public function __construct(
            RendererInterface $renderer, Router $router, BugTable $table, FlashService $flash, GameTable $gameTable, StatusBugTable $statusBugTable, SessionInterface $session, FileUpload $fileUpload, AttachmentTable $attachmentTable, SeverityTable $severityTable, CategoryTable $categoryTable, FrequencyTable $frequencyTable, CommentsTable $commentsTable, SendNotification $sendNotification, ContributorTable $contributorTable) {
        parent::__construct($renderer, $router, $table, $flash);

        $this->gameTable = $gameTable;
        $this->statusBugTable = $statusBugTable;
        $this->session = $session;
        $this->fileUpload = $fileUpload;
        $this->attachmentTable = $attachmentTable;
        $this->severityTable = $severityTable;
        $this->frequencyTable = $frequencyTable;
        $this->categoryTable = $categoryTable;
        $this->commentsTable = $commentsTable;
        $this->contributorTable = $contributorTable;
        $this->sendNotification = $sendNotification;
        $this->messages = [
            "create" => _("A bug report has been created."),
            "edit" => _("The bug report has been modified.")
        ];
    }

    public function deleteAttachment(ServerRequestInterface $request) {
        $params = $request->getParsedBody();
        $attachment = $this->attachmentTable->find($params["deleteAttachment"]);
        $this->fileUpload->delete($attachment->name);
        $this->attachmentTable->delete($params["deleteAttachment"]);

        $item = $this->table->find($request->getAttribute("id"));
        $attachments = $this->attachmentTable->findByBug($item->getId());
        Hydrator::hydrate($request->getParsedBody(), $item);
        return $this->renderer->render(
                        "@bug/edit", $this->formParams(compact("item", "attachments")));
    }

    public function edit(ServerRequestInterface $request) {
        $item = $this->table->find($request->getAttribute("id"));
        $game = $this->gameTable->find($item->getProjectId());
        $this->validateAccessRights($item->getUserId(), $game->getUserId(), $game->getId());
        $attachments = $this->attachmentTable->findByBug($item->getId());

        if ($request->getMethod() === "POST") {
            $params = $request->getParsedBody();
            if (isset($params["deleteAttachment"]) && $params["deleteAttachment"] !== null) {
                $this->deleteAttachment($request);
            } else {
                $validator = $this->getValidator($request);
                if ($validator->isValid()) {
                    $fields = $this->prePersist($request, $item);
                    array_pop($fields);
                    $this->table->update($item->getId(), $fields);
                    $this->uploadAttachments($request->getUploadedFiles(), $item->getId());
                    $this->flash->success($this->messages["edit"]);
                    $game = $this->gameTable->find($item->getProjectId());
                    $this->postPersist($request, $item);

                    //var_dump($item); die();
                    $this->sendNotification->send('change_bug', [
                        "gameId" => $item->getProjectId(),
                        "bug" => $item,
                        "list" => true
                    ]);

                    if ($item->getUserId() != $game->getUserId()) {
                        $this->sendNotification->send('changeContri', [
                            "gameId" => $game->getId(),
                            "bug" => $item,
                            "userId" => $item->getUserId()
                        ]);
                    }

                    if ($item->userProject == $this->session->get('auth.user')) {
                        return $this->redirect("bugs", [
                                    "id" => $item->getProjectId()
                        ]);
                    } else {
                        return $this->redirect("bug", [
                                    "id" => $item->getId()
                        ]);
                    }
                }
                $errors = $validator->getErrors();
                Hydrator::hydrate($request->getParsedBody(), $item);
            }
        }

        return $this->renderer->render(
                        "@bug/edit", $this->formParams(compact("item", "errors", "attachments")));
    }

    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $item->setProjectId($request->getAttribute('projectId'));
        $item->userProject = $this->gameTable->find($item->getProjectId())->getUserId();
        $item->projectSlug = $this->gameTable->find($item->getProjectId())->getSlug();

        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $this->table->insert($this->prePersist($request, $item));
                $item->setId($this->table->getPdo()->lastInsertId());
                $bug = $this->table->find($item->getId());
                $this->uploadAttachments($request->getUploadedFiles(), $item->getId());
                $this->postPersist($request, $item);
                $this->flash->success($this->messages["create"]);
                $this->sendNotification->send('new_bug', [
                    "gameId" => $item->getProjectId(),
                    "bug" => $bug,
                    "list" => true
                ]);

                if ($item->userProject == $this->session->get('auth.user')) {
                    return $this->redirect("bugs", [
                                "id" => $item->getProjectId()
                    ]);
                } else {
                    return $this->redirect("bug", [
                                "id" => $item->getId()
                    ]);
                }
            }
            Hydrator::hydrate($request->getParsedBody(), $item);
            $errors = $validator->getErrors();
        }

        return $this->renderer->render(
                        "@bug/create", $this->formParams(compact("item", "errors")));
    }

    public function delete(ServerRequestInterface $request) {
        $bugId = $request->getAttribute('id');
        $projectId = $request->getAttribute('projectId');
        $bug = $this->table->find($bugId);
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($bug->getUserId(), $game->getUserId(), $game->getId());
        $attachments = $this->attachmentTable->findBy("bug_id", $bugId)->fetchAll();
        $comments = $this->commentsTable->findBy("bug_id", $bugId)->fetchAll();
        $this->postPersist($request, $this->table->find($bugId));
        foreach ($comments->records as $comment) {
            $this->commentsTable->delete($comment["id"]);
        }
        foreach ($attachments->records as $attachment) {
            $this->fileUpload->delete($attachment["name"]);
            $this->attachmentTable->delete($attachment["id"]);
        }
        $this->table->delete($bugId);
        $this->flash->success(_("Successful deletion"));
        return $this->redirect("bugs", ['id' => $projectId]);
    }

    protected function formParams(array $params): array {
        $params["status"] = $this->statusBugTable->findList();
        $params["severity"] = $this->severityTable->findList();
        $params["frequency"] = $this->frequencyTable->findList();
        $params["category"] = $this->categoryTable->findList();
        return $params;
    }

    protected function getValidator(ServerRequestInterface $request) {

        $validator = parent::getValidator($request)
                ->required("title", "description")
                ->length("title", 2, 50)
                ->length("description", 10);

        return $validator;
    }

    protected function prePersist(ServerRequestInterface $request, $bug): array {
        $params = $request->getParsedBody();

        if ($this->session->get('auth.role')) {
            $userId = $this->session->get('auth.user');
        } else {
            $userId = null;
        }
        if (isset($params["projectId"])) {
            $params["project_id"] = $params["projectId"];
        }

        $params = array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
        return array_merge($params, ["updated_at" => date("Y-m-d H:i:s"),
            "created_at" => $bug->getCreatedAt()->format("Y-m-d H:i:s"),
            "user_id" => $userId]);
    }

    protected function getNewEntity() {
        $bug = new Bug();
        $bug->setCreatedAt(new \DateTime());
        return $bug;
    }

    protected function uploadAttachments($uploadedFile, $id) {
        foreach ($uploadedFile['attachments'] as $file) {
            $attachment = $this->fileUpload->upload($file);
            if ($attachment) {
                $this->attachmentTable->insert([
                    "name" => $attachment,
                    "created_at" => (new \DateTime())->format("Y-m-d H:i:s"),
                    "bug_id" => $id
                ]);
            }
        }
    }

    protected function postPersist(ServerRequestInterface $request, $item): void {
        $game = $this->gameTable->find($item->getProjectId());
        $this->gameTable->update($game->getId(), [
            "updated_at" => date("Y-m-d H:i:s")
        ]);
    }

    protected function validateAccessRights(int $bugUserId, int $gameUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null ||
                ($role == 'user' && $userId != $bugUserId && $userId != $gameUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
