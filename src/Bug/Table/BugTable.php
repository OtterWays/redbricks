<?php

namespace App\Bug\Table;

use App\Bug\Entity\Bug;
use Framework\Database\Query;
use Framework\Database\Table;
use App\Auth\UserTable;
use App\Bug\Table\StatusBugTable;
use App\Blog\Table\CategoryTable;
use App\Bug\Table\SeverityTable;
use App\Bug\Table\FrequencyTable;
use App\Blog\Table\GameTable;

class BugTable extends Table {

    protected $entity = Bug::class;
    protected $table = "bugs";

    /**
     * 
     * @return Query
     */
    public function findAll(): Query {
        $user = new UserTable($this->pdo);
        $status = new StatusBugTable($this->pdo);
        $frequency = new FrequencyTable($this->pdo);
        $severity = new SeverityTable($this->pdo);
        $category = new CategoryTable($this->pdo);
        $game = new GameTable($this->pdo);
        $name = 'name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("b.*, u.displayname as author, s.$name as statusName, "
                                . "f.$name as frequencyName, se.$name as severityName, c.$name as categoryName, g.user_id as userProject")
                        ->join($user->getTable() . " as u", "u.id = b.user_id")
                        ->join($status->getTable() . " as s", "s.id = b.status_id")
                        ->join($frequency->getTable() . " as f", "f.id = b.frequency_id")
                        ->join($severity->getTable() . " as se", "se.id = b.severity_id")
                        ->join($category->getTable() . " as c", "c.id = b.category_id")
                        ->join($game->getTable() . " as g", "g.id = b.project_id")
                        ->order("b.created_at DESC");
    }

    /**
     * 
     * @param int $gameId
     * @return Query
     */
    public function findShow(int $gameId): Query {
        return $this->findAll()->where("b.project_id = $gameId");
    }

    /**
     * 
     * @param int $id
     * @return bool|mixed
     * @throws NoRecordException
     */
    public function find(int $id) {
        return $this->findAll()->where("b.id = $id")->fetchOrFail();
    }

}
